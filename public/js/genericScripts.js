function dtTable(options) {
	$('#'+ options.id_tabela).DataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs float-right'l>r>"+
		"t"+
		"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"oLanguage": {
			"sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
		},
		"oLanguage": {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_ Registros por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Carregando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar: ",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			}
		},
		"bProcessing": true,
		"bServerSide": true,
		"ajax": {
			 "url": options.url_data,
			 "type": "GET"
		},
		"columns": options.colunas(),
		"order": options.ordernar
	});
}

function IsNotNullOrEmpty(obj, parameterToTest) {
    try {
        if (typeof parameterToTest == 'string' && parameterToTest !== "null" && parameterToTest !== "undefined" && parameterToTest !== "") obj = eval('obj.' + parameterToTest);
        return (typeof obj == 'function' && obj) || (typeof obj == 'boolean' && obj) || (typeof obj == 'undefined' && obj !== undefined) || (typeof obj == 'string' && obj !== "null" && obj !== "undefined" && obj !== "") || (typeof obj == 'object' && (obj != null && typeof obj.length != 'number') && !isEmptyObject(obj)) || (typeof obj == 'object' && typeof obj.length == 'number' && obj.length > 0) || (typeof obj == 'number' && obj > 0);
    } catch (e) {
        return false;
    }
}