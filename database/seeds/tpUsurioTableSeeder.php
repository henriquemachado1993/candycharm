<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class tpUsurioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tp_usuario')->insert([
            'primeiro_nom_usuario'    => 'Henrique',
            'complemento_nom_usuario' => 'Machado',
            'name'             => 'Henrique Machado',
            'email'               => 'henriquemachado1993@gmail.com',
            'password'               => bcrypt('123123'),
            'dhs_cadastro'            => '2017-06-23 14:20:40',
            'cod_usuario_operacao'    => 0,
        ]);
    }
}
