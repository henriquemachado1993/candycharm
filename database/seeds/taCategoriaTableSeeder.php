<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class taCategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ta_categoria')->insert([
            'nom_categoria' => 'Bolos',          
            'dsc_categoria' => 'Bolos',
            'dhs_cadastro'  => '2017-06-23 14:20:40',          
            'cod_usuario_operacao' => 0,         
        ]);

        DB::table('ta_categoria')->insert([
            'nom_categoria' => 'Bombons',          
            'dsc_categoria' => 'Bombons',
            'dhs_cadastro'  => '2017-06-23 14:20:40',          
            'cod_usuario_operacao' => 0,        
        ]);

        DB::table('ta_categoria')->insert([
            'nom_categoria' => 'Brigadeiros',          
            'dsc_categoria' => 'Brigadeiros',
            'dhs_cadastro'  => '2017-06-23 14:20:40',               
            'cod_usuario_operacao' => 0,       
        ]);

        DB::table('ta_categoria')->insert([
            'nom_categoria' => 'Docinhos',          
            'dsc_categoria' => 'Docinhos',
            'dhs_cadastro'  => '2017-06-23 14:20:40',                 
            'cod_usuario_operacao' => 0,        
        ]);

        DB::table('ta_categoria')->insert([
            'nom_categoria' => 'Salgados',          
            'dsc_categoria' => 'Salgados',
            'dhs_cadastro'  => '2017-06-23 14:20:40',                
            'cod_usuario_operacao' => 0,       
        ]);

        DB::table('ta_categoria')->insert([
            'nom_categoria' => 'Cupcake',          
            'dsc_categoria' => 'Cupcake',
            'dhs_cadastro'  => '2017-06-23 14:20:40',                 
            'cod_usuario_operacao' => 0,      
        ]);

        DB::table('ta_categoria')->insert([
            'nom_categoria' => 'Tortas',          
            'dsc_categoria' => 'Tortas',
            'dhs_cadastro'  => '2017-06-23 14:20:40',                 
            'cod_usuario_operacao' => 0,      
        ]);
    }
}
