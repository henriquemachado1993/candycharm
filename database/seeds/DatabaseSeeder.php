<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Aqui executa as seeders
        $this->call(taCategoriaTableSeeder::class);
        $this->call(tpUsurioTableSeeder::class);
    }
}
