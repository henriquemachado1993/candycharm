<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTdEnderecoEnvioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('td_endereco_envio', function (Blueprint $table) {
            $table->increments('cod_endereco_envio');

            $table->integer('id')->unsigned();
            $table->foreign('id')->references('id')->on('tp_usuario');

            $table->timestamp('dhs_cadastro');
            $table->timestamp('dhs_atualizacao')->nullable();
            $table->timestamp('dhs_exclusao_logica')->nullable();
            $table->integer('cod_usuario_operacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('td_endereco_envio');
    }
}
