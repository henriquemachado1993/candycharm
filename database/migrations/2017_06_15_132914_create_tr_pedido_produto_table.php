<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrPedidoProdutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_pedido_produto', function (Blueprint $table) {
            $table->increments('cod_pedido_produto');

            $table->integer('cod_pedido')->unsigned();
            $table->foreign('cod_pedido')->references('cod_pedido')->on('tp_pedido');
            $table->integer('cod_produto')->unsigned();
            $table->foreign('cod_produto')->references('cod_produto')->on('tp_produto');
                     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_pedido_produto');
    }
}
