<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTpPedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tp_pedido', function (Blueprint $table) {
            $table->increments('cod_pedido');

            $table->integer('id')->unsigned();
            $table->foreign('id')->references('id')->on('tp_usuario');

            $table->timestamp('dhs_cadastro');
            $table->timestamp('dhs_atualizacao')->nullable();
            $table->timestamp('dhs_exclusao_logica')->nullable();
            $table->integer('cod_usuario_operacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tp_encomenda');
    }
}
