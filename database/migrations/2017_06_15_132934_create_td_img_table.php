<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTdImgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('td_img', function (Blueprint $table) {
            $table->increments('cod_img');

            $table->integer('cod_produto')->unsigned();
            $table->foreign('cod_produto')->references('cod_produto')->on('tp_produto');

            $table->string('nom_img');
            $table->string('dsc_tamanho_img');
            $table->boolean('bol_capa');
          
            $table->timestamp('dhs_cadastro');
            $table->timestamp('dhs_atualizacao')->nullable();
            $table->timestamp('dhs_exclusao_logica')->nullable();
            $table->integer('cod_usuario_operacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('td_img');
    }
}
