<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTpUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tp_usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->rememberToken();

            $table->string('num_cpf', 15)->nullable();
            $table->string('name', 100);
            $table->string('primeiro_nom_usuario', 50);
            $table->string('complemento_nom_usuario', 100);
            $table->string('email', 100)->unique();
            $table->date('dat_nascimento')->nullable();
            $table->string('num_telefone', 15)->nullable();
            $table->string('num_celular', 15)->nullable();
            $table->string('password', 100);
            $table->string('permissao', 10)->nullable();

            $table->timestamp('dhs_cadastro');
            $table->timestamp('dhs_atualizacao')->nullable();
            $table->timestamp('dhs_exclusao_logica')->nullable();
            $table->integer('cod_usuario_operacao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tp_pedido');
        Schema::dropIfExists('tp_usuario');
    }
}
