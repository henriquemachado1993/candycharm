<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTpProdutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tp_produto', function (Blueprint $table) {
            $table->increments('cod_produto');

            $table->integer('cod_categoria')->unsigned();
            $table->foreign('cod_categoria')->references('cod_categoria')->on('ta_categoria');

            $table->string('nom_produto');
            $table->string('dsc_simples');
            $table->char('flg_oferta',1)->nullable();
            $table->text('dsc_completa');
            $table->decimal('vlr_produto', 10, 2);
          
            $table->timestamp('dhs_cadastro');
            $table->timestamp('dhs_atualizacao')->nullable();
            $table->timestamp('dhs_exclusao_logica')->nullable();
            $table->integer('cod_usuario_operacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tp_produto');
    }
}
