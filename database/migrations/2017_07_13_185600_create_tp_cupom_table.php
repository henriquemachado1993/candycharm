<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTpCupomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('tp_cupom', function (Blueprint $table) {
            $table->increments('cod_cupom');

            $table->string('nom_cupom', 50);
            $table->integer('percent_cupom');
            $table->boolean('flg_ativo');
          
            $table->timestamp('dhs_cadastro');
            $table->timestamp('dhs_atualizacao')->nullable();
            $table->timestamp('dhs_exclusao_logica')->nullable();
            $table->integer('cod_usuario_operacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tp_cupom');
    }
}
