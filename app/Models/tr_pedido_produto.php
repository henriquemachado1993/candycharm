<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class tr_pedido_produto extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [];
    protected $hidden = [];
    protected $table = 'tr_pedido_produto';
    protected $primaryKey = 'cod_pedido_produto';
    protected $softDelete = true;
    protected $dateFormat = 'Y-m-d H:i:s';

    const CREATED_AT = 'dhs_cadastro';
    const UPDATED_AT = 'dhs_atualizacao';
    const DELETED_AT = 'dhs_exclusao_logica';

    public function getAll()
    {
        $objReg = DB::table('tr_pedido_produto')                         
                            ->whereNull('tr_pedido_produto.dhs_exclusao_logica')                                                  
                            ->get();
        return $objReg;
    }
}
