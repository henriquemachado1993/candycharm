<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class tp_cupom extends Model
{
     use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'nom_cupom',
        'percent_cupom',
        'flg_ativo',
        'dhs_cadastro',
        'dhs_atualizacao',
        'dhs_exclusao_logica',        
        'cod_usuario_operacao'
    ];

    protected $guarded    = [];
    protected $hidden     = [];
    protected $table      = 'tp_cupom';
    protected $primaryKey = 'cod_cupom';
    protected $softDelete = true;
    protected $dateFormat = 'Y-m-d H:i:s';

    const CREATED_AT = 'dhs_cadastro';
    const UPDATED_AT = 'dhs_atualizacao';
    const DELETED_AT = 'dhs_exclusao_logica';

    //regras para validação de dados
    public $rules = [
        'nom_cupom' => 'required|string|max:15',
        'percent_cupom' => 'required|string'
    ];

    public $messages = [
        'nom_cupom.required' => 'O campo Nome do Cupom é obrigatório.',
        'nom_cupom.max'      => 'O campo Nome do Cupom não pode ser superior a 15 caracteres..',
        'percent_cupom.required' => 'O campo Desconto é obrigatório.',
    ];
    
    //CARREGA TODAS AS CATEGORIAS
    // public function getTodasCategorias(){
    //     return DB::table('ta_categoria')
    //                 ->leftJoin('tp_produto', 'ta_categoria.cod_categoria', '=', 'tp_produto.cod_categoria')
    //                 ->select(
    //                     'ta_categoria.cod_categoria', 
    //                     'ta_categoria.nom_categoria',
    //                     'ta_categoria.dsc_categoria',
    //                     'tp_produto.cod_categoria',
    //                     DB::raw('count(tp_produto.cod_categoria) as "total"')
    //                 )                   
    //                 ->groupBy(
    //                     'ta_categoria.cod_categoria', 
    //                     'ta_categoria.nom_categoria',
    //                     'ta_categoria.dsc_categoria',
    //                     'tp_produto.cod_categoria'
    //                 )
    //                 ->orderBy('ta_categoria.nom_categoria', 'asc')
    //                 ->whereNull('ta_categoria.dhs_exclusao_logica')     
    //                 ->get();
    // }

    //Carrega um cupom especifico
    public function getCupom( $id ){        
        return DB::table('tp_cupom')                    
                    ->where('tp_cupom.cod_cupom','=', (string)$id )
                    ->whereNull('tp_cupom.dhs_exclusao_logica')     
                    ->first();
    }

    //CARREGA UM CUPOM PELO NOME
    public function getCupomNome( $nomeCupom ){
        return DB::table('tp_cupom')                    
                    ->where('tp_cupom.nom_cupom','=', strtoupper($nomeCupom) )
                    ->whereNull('tp_cupom.dhs_exclusao_logica')     
                    ->first();
    }

    //CARREGA TODAS AS CATEGORIAS
    // public function getCategorias(){
    //     return DB::table('ta_categoria')
    //                 ->select(
    //                     'ta_categoria.cod_categoria', 
    //                     'ta_categoria.nom_categoria',
    //                     'ta_categoria.dsc_categoria'
    //                 ) 
    //                 ->orderBy('ta_categoria.nom_categoria', 'asc')
    //                 ->whereNull('ta_categoria.dhs_exclusao_logica')     
    //                 ->get();
    // }

    //CARREGA TODAS AS CATEGORIAS PARA O DATATABLE
    public function getDtCupom(){
        $objReturn = DB::table('tp_cupom')
                            ->select(
                                'tp_cupom.cod_cupom',
                                'tp_cupom.nom_cupom',
                                'tp_cupom.percent_cupom',
                                'tp_cupom.flg_ativo'
                            )
                            ->whereNull('tp_cupom.dhs_exclusao_logica')                                                     
                            ->get();
        
        return Datatables::of($objReturn)
                ->make(true);
    }
}
