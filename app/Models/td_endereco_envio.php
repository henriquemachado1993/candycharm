<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class td_endereco_envio extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [];
    protected $hidden = [];
    protected $table = 'td_endereco_envio';
    protected $primaryKey = 'COD_ENDERECO_ENVIO';
    protected $softDelete = true;
    protected $dateFormat = 'Y-m-d H:i:s';

    const CREATED_AT = 'dhs_cadastro';
    const UPDATED_AT = 'dhs_atualizacao';
    const DELETED_AT = 'dhs_exclusao_logica';
    
    public function getAll()
    {
        $objReg = DB::table('td_endereco_envio')                         
                            ->whereNull('td_endereco_envio.dhs_exclusao_logica')                                                   
                            ->get();
        return $objReg;
    }
}
