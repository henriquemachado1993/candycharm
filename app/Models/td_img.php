<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class td_img extends Model
{
    use SoftDeletes;
    use Notifiable;

     protected $fillable = [        
        'cod_produto',
        'nom_img',
        'dsc_tamanho_img',
        'bol_capa',
        'dhs_cadastro',
        'dhs_atualizacao',
        'dhs_exclusao_logica',
        'cod_usuario_operacao'
    ];

    protected $guarded    = [];
    protected $hidden     = [];
    protected $table      = 'td_img';
    protected $primaryKey = 'cod_img';
    protected $softDelete = true;
    protected $dateFormat = 'Y-m-d H:i:s';

    const CREATED_AT = 'dhs_cadastro';
    const UPDATED_AT = 'dhs_atualizacao';
    const DELETED_AT = 'dhs_exclusao_logica';

    //regras para validação de dados
    public $rules = [
        'nom_imagem.*' => 'required|mimes:png,jpg,jpeg'
    ];

    public $messages = [
        'nom_imagem.*.required' => 'É obrigatório selecionar pelo menos uma imagem.',
        'nom_imagem.*.mimes'    => 'Somente é permitidos imagens utilizando os formatos (PNG, JPG, JPEG).'
    ];
       
    public function getProduto()
    {
        return $this->belongsTo('App\Models\tp_produto', 'cod_produto', 'cod_produto');
    }
}
