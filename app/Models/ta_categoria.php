<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class ta_categoria extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'nom_categoria',
        'dsc_categoria',
        'dhs_cadastro',
        'dhs_atualizacao',
        'dhs_exclusao_logica',        
        'cod_usuario_operacao'
    ];

    protected $guarded    = [];
    protected $hidden     = [];
    protected $table      = 'ta_categoria';
    protected $primaryKey = 'cod_categoria';
    protected $softDelete = true;
    protected $dateFormat = 'Y-m-d H:i:s';

    const CREATED_AT = 'dhs_cadastro';
    const UPDATED_AT = 'dhs_atualizacao';
    const DELETED_AT = 'dhs_exclusao_logica';

    //regras para validação de dados
    public $rules = [
        'nom_categoria' => 'required|string|max:50',
        'dsc_categoria' => 'required|string'
    ];

    public $messages = [
        'nom_categoria.required' => 'O campo categoria é obrigatório.',
        'nom_categoria.max'      => 'O campo nome categoria não pode ser superior a 50 caracteres..',
        'dsc_categoria.required' => 'O campo descrição da categoria é obrigatório.',
    ];
    
    public function getProdutos()
    {
        return $this->hasMany('App\Models\tp_produto', 'cod_categoria', 'cod_categoria');
    }

    //CARREGA TODAS AS CATEGORIAS
    public function getTodasCategorias(){
        return DB::table('ta_categoria')
                    ->leftJoin('tp_produto', 'ta_categoria.cod_categoria', '=', 'tp_produto.cod_categoria')
                    ->select(
                        'ta_categoria.cod_categoria', 
                        'ta_categoria.nom_categoria',
                        'ta_categoria.dsc_categoria',
                        'tp_produto.cod_categoria',
                        DB::raw('count(tp_produto.cod_categoria) as "total"')
                    )                   
                    ->groupBy(
                        'ta_categoria.cod_categoria', 
                        'ta_categoria.nom_categoria',
                        'ta_categoria.dsc_categoria',
                        'tp_produto.cod_categoria'
                    )
                    ->orderBy('ta_categoria.nom_categoria', 'asc')
                    ->whereNull('ta_categoria.dhs_exclusao_logica')     
                    ->get();
    }

    //CARREGA UMA CATEGORIA ESPECIFICA
    public function getCategoria( $id ){
        return DB::table('ta_categoria')                    
                    ->where('ta_categoria.cod_categoria','=', (string)$id )
                    ->whereNull('ta_categoria.dhs_exclusao_logica')     
                    ->first();
    }

    //CARREGA TODAS AS CATEGORIAS
    public function getCategorias(){
        return DB::table('ta_categoria')
                    ->select(
                        'ta_categoria.cod_categoria', 
                        'ta_categoria.nom_categoria',
                        'ta_categoria.dsc_categoria'
                    ) 
                    ->orderBy('ta_categoria.nom_categoria', 'asc')
                    ->whereNull('ta_categoria.dhs_exclusao_logica')     
                    ->get();
    }

    //CARREGA TODAS AS CATEGORIAS PARA O DATATABLE
    public function getDtCategoria(){
        $objReturn = DB::table('ta_categoria')
                            ->select(
                                'ta_categoria.cod_categoria',
                                'ta_categoria.nom_categoria',
                                'ta_categoria.dsc_categoria'
                            )
                            ->whereNull('ta_categoria.dhs_exclusao_logica')                                                     
                            ->get();
        
        return Datatables::of($objReturn)
                ->make(true);
    }

}
