<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class tp_produto extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [        
        'cod_categoria',
        'nom_produto',
        'dsc_simples',
        'dsc_completa',
        'flg_oferta',
        'vlr_produto',
        'dhs_cadastro',
        'dhs_atualizacao',
        'dhs_exclusao_logica',
        'cod_usuario_operacao'
    ];

    protected $guarded    = [];
    protected $hidden     = [];
    protected $table      = 'tp_produto';
    protected $primaryKey = 'cod_produto';
    protected $softDelete = true;
    protected $dateFormat = 'Y-m-d H:i:s';

    const CREATED_AT = 'dhs_cadastro';
    const UPDATED_AT = 'dhs_atualizacao';
    const DELETED_AT = 'dhs_exclusao_logica';
    
    //regras para validação de dados
    public $rules = [
        'nom_produto'   => 'required|string',
        'dsc_simples'   => 'required|string',
        'dsc_completa'  => 'required|string',
        'cod_categoria' => 'required|string',        
        'vlr_produto'   => 'required|string',
        'nom_imagem.*'  => 'required|mimes:png,jpg,jpeg'
    ];

    //mensagens customizadas
    public $messages = [
        'nom_produto.required'   => 'O campo categoria é obrigatório.',
        'dsc_simples.required'   => 'O campo descrição simples é obrigatório.',
        'dsc_completa.required'  => 'O campo descrição completa é obrigatória.',
        'cod_categoria.required' => 'O campo categoria é obrigatório.',
        'vlr_produto.required'   => 'O campo valor do produto é obrigatório.',       
        'nom_imagem.*.required'  => 'É obrigatório selecionar pelo menos uma imagem.',
        'nom_imagem.*.mimes'     => 'Somente é permitidos imagens utilizando os formatos (PNG, JPG, JPEG).'
    ];

    //Relacionamentos
    public function getImgsProduto()
    {
        return $this->hasMany('App\Models\td_img', 'cod_produto', 'cod_produto');
    }

    public function getCategorias()
    {
        return $this->belongsTo('App\Models\ta_categoria', 'cod_categoria', 'cod_categoria');
    }
    
    //Carrega todos os produtos ou os produtos de uma categoria específica
    public function getTodosProdutos($codCategoria){
       
        if(is_null($codCategoria)){
            $produto = $this::with('getImgsProduto')
                        ->orderBy('nom_produto', 'asc')
                        ->paginate(9);
        }else{
            $produto = $this::with('getImgsProduto')
                        ->where('cod_categoria', "=" , $codCategoria)
                        ->orderBy('nom_produto', 'asc')
                        ->paginate(9);
        }

        return $produto;
    }

    //Carrega todos os produtos em oferta
    public function getProdutoOfertas($qntPorPaginas = 9){
        return $this::with('getImgsProduto')
                        ->where('flg_oferta','=','s')
                        ->orderBy('nom_produto', 'asc')
                        ->paginate($qntPorPaginas);
    }

    //Carrega todos os produtos para o datatable
    public function getDtProdutos(){
        $objReturn = $this::with('getImgsProduto')                                    
                            ->get();

        return Datatables::of($objReturn)
                                ->make(true);
    }

    //Carrega um produto específico
    public function getProduto(string $codProduto){
        return $this::with('getImgsProduto')
                    ->with('getCategorias')
                        ->where('cod_produto', $codProduto)
                        ->first();
    }

    //Busca principal do site
    public function buscaPrincipal($valorDaBusca){
       
        $produto = $this::with('getImgsProduto')
                    ->where('nom_produto', 'like', '%'.$valorDaBusca.'%')
                    ->orderBy('nom_produto', 'asc')
                    ->paginate(12);

        return $produto;
    }
}
