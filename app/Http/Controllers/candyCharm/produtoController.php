<?php

namespace App\Http\Controllers\candyCharm;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \App\Models\tp_produto;
use \App\Models\ta_categoria;

class produtoController extends Controller
{
    //Metodo construtor
    private $produto;
    private $categoria;    
    public function __construct(tp_produto $produto, ta_categoria $categoria){
        $this->produto = $produto;        
        $this->categoria = $categoria;
    }

    public function buscaPrincipal(Request $request)
    {   
        
        $objReturn['produto'] = $this->produto->buscaPrincipal(strtoupper($request->input('textoBusca')));
        $objReturn['codPagina'] = "";
        $objReturn['breadcrumbs'] = $this->breadcrumbs([            
            ['pagina'=>'Resultado da busca', 'rota'=>'']
        ]);

        return view('candycharm.produtosResultadoBusca', ['objReturn' => $objReturn]);
    }

	public function getProdutos(Request $request)
    {   
        //Carrega categorias e todos os produtos ou de uma categoria especifica 
        $objReturn['categoria'] = $this->categoria->getTodasCategorias();       
        $objReturn['produto']   = $this->produto->getTodosProdutos($request->codCategoria);
       
        //Codigo da pagina para o botão do menu ficar marcado
        $objReturn['codPagina'] = 'produtos';        
        //herarquia das paginas
        if(is_null($request->codCategoria)){
            $objReturn['breadcrumbs'] = $this->breadcrumbs([            
                ['pagina'=>'Produtos', 'rota'=>'']
            ]);
        }else{
            $objReturn['breadcrumbs'] = $this->breadcrumbs([            
                ['pagina'=>'Produtos', 'rota'=>'produtos'],
                ['pagina'=>'Categoria', 'rota'=>'']
            ]);
        }
        
        return view('candycharm.produtos', ['objReturn' => $objReturn]);
    }

   	public function getProdutoDetalhe(Request $request)
    {
        //Carrega categorias e um produto especifico
        $objReturn['categoria'] = $this->categoria->getTodasCategorias();                               
        $objReturn['produto']   = $this->produto->getProduto($request->codProduto);
              
        //Codigo da pagina para o botão do menu ficar marcado
        $objReturn['codPagina'] = "";
        //herarquia das paginas
        $objReturn['breadcrumbs'] = $this->breadcrumbs([            
            ['pagina'=>'Produtos', 'rota'=>'produtos'],
            ['pagina'=> $objReturn['produto']->nom_produto , 'rota'=>'']
        ]);

        return view('candycharm.produtoDetalhe', ['objReturn' => $objReturn]);        
    }
}
