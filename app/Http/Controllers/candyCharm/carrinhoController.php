<?php

namespace App\Http\Controllers\candyCharm;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use \Cart;

use \App\Models\tp_produto;
use \App\Models\ta_categoria;
use \App\Models\tp_cupom;

class carrinhoController extends Controller
{
    //Metodo construtor
    private $produto;
    private $categoria;    
    private $cupom;    
    public function __construct(tp_produto $produto, ta_categoria $categoria, tp_cupom $cupom){
        $this->produto = $produto;        
        $this->categoria = $categoria;
        $this->cupom = $cupom;
    }

    /**
     * add item to the cart, it can be an array or multi dimensional array
     *
     * @param string|array $id
     * @param string $name
     * @param float $price
     * @param int $quantity
     * @param array $attributes
     * @param CartCondition|array $conditions
     * @return $this
     * @throws InvalidItemException
     */

    //Adiciona produtos ao carrinho
    public function adicionaProdutoCarrinho(Request $request)
    {   
        if(!is_null($request->input('cod_produto')) && !is_null($request->input('qnt_produto'))  ){
            
            $prod = $this->produto->getProduto($request->input('cod_produto'));

            $item = array(
                'id' => $prod->cod_produto,
                'name' => $prod->nom_produto,
                'price' => $prod->vlr_produto,
                'quantity' => $request->input('qnt_produto'),
                'attributes' => array()
            );

            Cart::add($item);

            $objReturn['codPagina'] = "";
            $objReturn['breadcrumbs'] = $this->breadcrumbs([            
                ['pagina'=>'Carrinho de compras', 'rota'=>'']
            ]);

            return redirect()->route('candycharm.carrinho');

        }else if(!is_null($request->input('cod_produto'))){

            $objReturn['categoria'] = $this->categoria->getTodasCategorias();     
            $objReturn['produto'] = $this->produto->getProduto($request->input('cod_produto'));

            $this->messagemCustom("Não foi possível adicionar o produto ao carrinho de compras.","danger");

            $objReturn['codPagina'] = "";
            //herarquia das paginas
            $objReturn['breadcrumbs'] = $this->breadcrumbs([            
                ['pagina'=>'Produtos', 'rota'=>'produtos'],
                ['pagina'=> $objReturn['produto']->nom_produto , 'rota'=>'']
            ]);

            return view('candycharm.produtoDetalhe', ['objReturn' => $objReturn]);     
        }
    } 

    //Altera quantidade de um produto específico do carrinho
    public function alteraQntProdutoCarrinho(Request $request)
    {
        if(!is_null($request->input('cod_produto')) && !is_null($request->input('qnt_produto'))  ){           
            Cart::update($request->input('cod_produto'), array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->input('qnt_produto')
                ),
            ));

        }else if(!is_null($request->input('cod_produto'))){
            $this->messagemCustom("Não foi possível atualizar a quantidade do produto.","danger");            
        }    

        return redirect()->route('candycharm.carrinho');
    } 

    //Remove um produto específico do carrinho
    public function removerProdutoCarrinho($id)
    {
        Cart::remove($id);        
        return redirect()->route('candycharm.carrinho');  
    }

    //Exclui todos os produtos do carrinho
    public function limparCarrinho()
    {
        Cart::clear();
        return redirect()->route('candycharm.carrinho');  
    }

    //Valor total de todos os produtos
    public function getTotal()
    {   
        $total = Cart::getTotal();
        return $total;        
    }

    //Carrega intens do carrinho
    public function getCarrinho()
    {
        $cartCollection = Cart::getContent();
        $objReturn['produtosCarrinho'] = $cartCollection->toArray();
        
        $objReturn['vlrTotalCarrinho'] = Cart::getTotal();
        
        // foreach ($cartCollection as $key => $value) {
        //     $objReturn['vlrTotalCarrinho'] = $value->getPriceSum();
        // }

        //Codigo da pagina para o botão do menu ficar marcado
        $objReturn['codPagina'] = "";
        //herarquia das paginas
        $objReturn['breadcrumbs'] = $this->breadcrumbs([            
            ['pagina'=>'Carrinho', 'rota'=>'']
        ]);
        
        return view('candycharm.carrinhoDeCompras', ['objReturn' => $objReturn]);        
    }

    public function aplicarCupom(Request $request)
    {
        $this->validate($request,[
            'nom_cupom'     => 'required|string'                    
        ], [
            'nom_cupom.required'   => 'Para aplicar é necessário o CUPOM.',
            'email.string'   => 'O campo aceita somente dados do tipo texto.'                  
        ]);

        $objCupom = $this->cupom->getCupomNome($request->input('nom_cupom'));
        
        if(!is_null($objCupom)){

            $cartCollection = Cart::getContent();
            $produtosCarrinho = $cartCollection->toArray();

            if($cartCollection->count() > 0){
                
                //Preenche os dados do cupom para ser aplicado
                $cupom = new \Darryldecode\Cart\CartCondition(array(
                    'name' => $objCupom->nom_cupom,
                    'type' => 'coupon',
                    'target' => 'item',
                    'value' => '-'.$objCupom->percent_cupom.'%',
                ));

                //Itera sobre cada produto e aplica o desconto
                foreach ($produtosCarrinho as $key => $value) {                    
                    
                    //Verifica se algum cupom já foi aplicado
                    if(count($value['conditions']) >= 1){
                        $this->messagemCustom("Um cupom já foi aplicado a sua compra.","warning");   
                        return redirect()->route('candycharm.carrinho');
                    }

                    Cart::addItemCondition($value['id'], $cupom);
                }

            }else{
                $this->messagemCustom("Não foi possível aplicar o cupom, o carrinho está vazio.","danger"); 
            }
        }else{
            $this->messagemCustom("O cupom preenchido é inválido.","danger");      
        }

        return redirect()->route('candycharm.carrinho');
    }  
    
}


 