<?php

namespace App\Http\Controllers\candyCharm;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class empresaController extends Controller
{
	public function getSobreNos(Request $request)
    {
        //Codigo da pagina para o botão do menu ficar marcado
        $objReturn['codPagina'] = 'sobreNos';
        //herarquia das paginas
        $objReturn['breadcrumbs'] = $this->breadcrumbs([            
            ['pagina'=>'Sobre nós', 'rota'=>'']
        ]);
        
        return view('candycharm.sobreNos', ['objReturn' => $objReturn]);      
    }

    public function enviarEmail(Request $request)
    {   
        //Valida form
        $this->validate($request,[
            'nome'     => 'required|string',
            'email'    => 'required|email',
            'mensagem' => 'required|string'           
        ], [
            'nome.required'   => 'O campo Nome é obrigatório.',
            'email.required'   => 'O campo E-mail é obrigatório.',
            'mensagem.required'  => 'O campo Mensagem é obrigatório.',           
            'email.email'     => 'E-mail inválido.'          
        ]);

        Mail::send('emails.emailContato', $request->All(), function($msg){
            $msg->subject(config('constants.email.tituloEmail'));
            $msg->to(config('constants.email.enderecoEmail'), 'CandyCharm LTDA');
        });

        $this->messagem(config('constants.msg.emailSuccess'));

        return redirect()->route('candycharm.sac');
    }

    public function getSac(Request $request)
    {    	
        //Codigo da pagina para o botão do menu ficar marcado
        $objReturn['codPagina'] = 'sac';
        //herarquia das paginas
        $objReturn['breadcrumbs'] = $this->breadcrumbs([            
            ['pagina'=>'SAC', 'rota'=>'']
        ]);
        
        return view('candycharm.sac', ['objReturn' => $objReturn]);
    }    
}
