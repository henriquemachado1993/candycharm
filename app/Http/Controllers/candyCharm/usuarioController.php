<?php

namespace App\Http\Controllers\candyCharm;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\tp_usuario;

class usuarioController extends Controller
{
    //Metodo construtor
    private $usuario;
    public function __construct(tp_usuario $usuario){
        $this->usuario = $usuario;
    }

    public function getUsuario( Request $request,  $num_ses )
    {    
        return view('candycharm.produtos');
    }
    
    public function getMeusPedidos()
    {        
        //Codigo da pagina para o botão do menu ficar marcado
        $objReturn['codPagina'] = 'meusPedidos';
        //herarquia das paginas
        $objReturn['breadcrumbs'] = $this->breadcrumbs([            
            ['pagina'=>'Meus pedidos', 'rota'=>'']
        ]);
        
        return view('candycharm.meusPedidos', ['objReturn' => $objReturn]);      
    }

    public function getMeusDados()
    {        
        //Codigo da pagina para o botão do menu ficar marcado
        $objReturn['codPagina'] = 'meusDados';
        //herarquia das paginas
        $objReturn['breadcrumbs'] = $this->breadcrumbs([            
            ['pagina'=>'Dados cadastrais', 'rota'=>'']
        ]);
        
        return view('candycharm.usuarioMeusDados', ['objReturn' => $objReturn]);      
    }

    public function getAlterarSenha()
    {
        //Codigo da pagina para o botão do menu ficar marcado
        $objReturn['codPagina'] = 'alterarSenha';
        //herarquia das paginas
        $objReturn['breadcrumbs'] = $this->breadcrumbs([            
            ['pagina'=>'Alterar senha', 'rota'=>'/alterar-senha']
        ]);

        return view('candycharm.usuarioAlterarSenha', ['objReturn' => $objReturn]);
    }

    public function AlterarSenha(Request $request)
    {
        //Valida os dados do formulário
        $this->validate( $request, $this->usuario->rules_alterar_senha, $this->usuario->messages_alterar_senha );

        $data = $request->All();
        $objReturn = $this->usuario->find($this->usuarioOperacao());

        if(Hash::check( $data['password_atual'] , $objReturn->password)){
            $update = $objReturn->update([
                'password' => bcrypt($data['password'])           
            ]);

            if($update){
                $this->messagem(config('constants.msg.updateSuccess'));
            }else{
                $this->messagem(config('constants.msg.updateError'));
            }
        }else{
            $this->messagemCustom("A senha atual não confere.","danger");
        }

        return redirect()->route('candycharm.alterar-senha');
    }
    
}
