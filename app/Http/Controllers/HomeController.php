<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\tp_produto;
use App\Models\td_img;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //Metodo construtor
    private $produto;
    private $img;
    public function __construct(tp_produto $produto, td_img $img){
        $this->produto = $produto;
        $this->img = $img;

        //Bloqueia o acesso se não tiver logado
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objReturn['ofertas'] = $this->produto->getProdutoOfertas(8);     
        $objReturn['codPagina'] = 'home';     
        
        return view('candyCharm.inicio', ['objReturn' => $objReturn]);
    }

    public function indexAdmin()
    {  
        return view('candyCharmAdmin.painelDeControle');
    }
}
