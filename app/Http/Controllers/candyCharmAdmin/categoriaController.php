<?php

namespace App\Http\Controllers\candyCharmAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\ta_categoria;

class categoriaController extends Controller
{
    //Metodo construtor
    private $categoria;
    public function __construct(ta_categoria $categoria){
        $this->categoria = $categoria;
    }

    public function getInserir()
    {   
        return view('candyCharmAdmin.categoriaInserir');
    }

    public function getAlterar($id = 000)
    {   
        $objReturn = $this->categoria->getCategoria($id);

        if (is_null($objReturn)){
             $this->messagem(config('constants.msg.regNotFound'));
        }

        return view('candyCharmAdmin.categoriaAlterar', [ 'objReturn' => $objReturn ]);
    }

    public function getDetalhe($id = 000)
    {           
        $objReturn = $this->categoria->getCategoria($id);

        if (is_null($objReturn)){
             $this->messagem(config('constants.msg.regNotFound'));
        }
        
        return view('candyCharmAdmin.categoriaDetalhe', [ 'objReturn' => $objReturn ]);
    }

    public function getListaCategorias()
    {   
        return view('candyCharmAdmin.categoriaListar');
    }

    public function getListaCategoriasJson(){
        return $this->categoria->getDtCategoria();
    }

    public function inserirCategoria(Request $request)
    {   
        //Valida os dados do formulário
        $this->validate( $request, $this->categoria->rules, $this->categoria->messages );

        $objReturn = $this->categoria->create([
            'nom_categoria' => $request->input('nom_categoria'),
            'dsc_categoria' => $request->input('dsc_categoria'),
            'cod_usuario_operacao' => $this->usuarioOperacao()
        ]);

        if($objReturn){
            $this->messagem(config('constants.msg.insertSuccess'));
            return redirect()->route('administracao.categoria.detalhe', [ 'objReturn' => $objReturn ]);
        }else{
            $this->messagem(config('constants.msg.insertError'));
            return view('candyCharmAdmin.categoriaInserir');
        }
    }

    public function alterarCategoria(Request $request)
    {   
        //Valida os dados do formulário
        $this->validate( $request, $this->categoria->rules, $this->categoria->messages );
        
        $objReturn = $this->categoria->find($request->input('id'));
        $update = $objReturn->update([
            'nom_categoria' => $request->input('nom_categoria'),
            'dsc_categoria' => $request->input('dsc_categoria'),
            'cod_usuario_operacao' => $this->usuarioOperacao()
        ]);         

        if($update){
            $this->messagem(config('constants.msg.updateSuccess'));
        }else{
            $this->messagem(config('constants.msg.updateError'));
        }

        return redirect()->route('administracao.categoria.detalhe', [ 'objReturn' => $objReturn ]);
    } 

    public function excluirCategoria( String $id )
    {          
        $objReturn = $this->categoria->find($id);
        $update = $objReturn->delete();         

        if($update){
            $this->messagem(config('constants.msg.deleteSuccess'));
        }else{
            $this->messagem(config('constants.msg.deleteError'));
        }

        return redirect()->route('administracao.categoria.lista-categorias');
    }  

}