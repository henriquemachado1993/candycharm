<?php

namespace App\Http\Controllers\candyCharmAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\tp_produto;
use App\Models\td_img;
use App\Models\ta_categoria;

class produtoController extends Controller
{
    //Metodo construtor
    private $produto;
    private $img;
    private $categoria;
    public function __construct(tp_produto $produto, td_img $img, ta_categoria $categoria){
        $this->produto = $produto;
        $this->img = $img;
        $this->categoria = $categoria;
    }

    public function getInserir()
    {
        //Preenche o select das categorias da view de inserir produtos
        $objReturn['categoria'] = $this->categoria->getCategorias();
        
        return view('candyCharmAdmin.produtoInserir', ['objReturn'=>$objReturn]);
    }

    public function getAlterar()
    {   
        return view('candyCharmAdmin.produtoAlterar');
    }

    public function getDetalhe($id = 000)
    {
        $objReturn = $this->produto->getProduto($id);
        return view('candyCharmAdmin.produtoDetalhe',['objReturn'=>$objReturn]);
    }

    public function getListaProdutos()
    {   
        return view('candyCharmAdmin.produtoListar');
    }

    public function getListaProdutosJson(){
        return $this->produto->getDtProdutos();
    }

    public function inserirProduto(Request $request)
    {   
        //Valida os dados do formulário
        $validator = validator( $request->All(), $this->produto->rules, $this->produto->messages);
        if ($validator->fails()) {
            return redirect('/administracao/produto/inserir')
                    ->withErrors($validator)
                    ->withInput();
        }
                
        $objReturn = $this->produto->create([
            'cod_categoria' => $request->input('cod_categoria'),
            'nom_produto' => strtoupper($request->input('nom_produto')),
            'dsc_simples' => $request->input('dsc_simples'),
            'dsc_completa' => $request->input('dsc_completa'),
            'flg_oferta'=> ( Input::has('flg_oferta') ? $request->input('flg_oferta') : 'n' ),
            'vlr_produto' => $this->moedaReal($request->input('vlr_produto')),
            'cod_usuario_operacao' => $this->usuarioOperacao()
        ]);

        //Verifica se o produto foi inserido
        if ($objReturn) {

            $cod_produto = $objReturn->cod_produto;

            //Insere as imagens
            foreach ($request->nom_imagem as $file) {
                //TODO: GERAR HASH DOS BITES DA IMAGEM
                $img_name_original =  time().'_'.$file->getClientOriginalName();
                
                //Insere nome da imagem no banco de dados
                $objReturnImg = $this->img->create([
                    'cod_produto' =>  $cod_produto,
                    'nom_img' => $img_name_original,
                    'dsc_tamanho_img' => '100',
                    'bol_capa' => false,
                    'cod_usuario_operacao' => $this->usuarioOperacao()
                ]);

                if($objReturnImg){
                    //Insere a imagem no servidor
                    $file->move(public_path('/imagens/produtos'),  $img_name_original);
                }
            }
        }
        
        if($objReturn){
            $this->messagem(config('constants.msg.insertSuccess'));
            return redirect()->route('administracao.produto.detalhe', [ 'objReturn' => $objReturn ]);
        }else{
            $this->messagem(config('constants.msg.insertError'));
            return view('candyCharmAdmin.produtoInserir');
        }
    }
}
