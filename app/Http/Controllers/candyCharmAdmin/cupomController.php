<?php

namespace App\Http\Controllers\candyCharmAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\tp_cupom;

class cupomController extends Controller
{
	//Metodo construtor
    private $cupom;
    public function __construct(tp_cupom $cupom){
        $this->cupom = $cupom;
    }

    public function getInserir()
    {   
        return view('candyCharmAdmin.cupomInserir');
    }

    public function getAlterar($id = 000)
    {   
        $objReturn = $this->cupom->getCupom($id);

        if (is_null($objReturn)){
             $this->messagem(config('constants.msg.regNotFound'));
        }

        return view('candyCharmAdmin.cupomAlterar', [ 'objReturn' => $objReturn ]);
    }

    public function getDetalhe($id = 000)
    {           
        $objReturn = $this->cupom->getCupom($id);

        if (is_null($objReturn)){
             $this->messagem(config('constants.msg.regNotFound'));
        }
        
        return view('candyCharmAdmin.cupomDetalhe', [ 'objReturn' => $objReturn ]);
    }

    public function getListaCupons()
    {   
        return view('candyCharmAdmin.cupomListar');
    }

    public function getListaCuponsJson(){
        return $this->cupom->getDtCupom();
    }

    public function inserirCupom(Request $request)
    {   
        //Valida os dados do formulário
        $this->validate( $request, $this->cupom->rules, $this->cupom->messages );

        //TODO: remover espaços do nome do cupom quando for inserir

        $objReturn = $this->cupom->create([
            'nom_cupom' => strtoupper($request->input('nom_cupom')),
            'percent_cupom' => $request->input('percent_cupom'),
            'flg_ativo' =>  true,
            'cod_usuario_operacao' => $this->usuarioOperacao()
        ]);

        if($objReturn){
            $this->messagem(config('constants.msg.insertSuccess'));
            return redirect()->route('administracao.cupom.detalhe', [ 'objReturn' => $objReturn ]);
        }else{
            $this->messagem(config('constants.msg.insertError'));
            return view('candyCharmAdmin.cupomInserir');
        }
    }

    public function alterarCupom(Request $request)
    {   
        $this->validate( $request, $this->cupom->rules, $this->cupom->messages );
        
        $objReturn = $this->cupom->find($request->input('id'));
        $update = $objReturn->update([
            'nom_cupom' => strtoupper($request->input('nom_cupom')),
            'percent_cupom' => $request->input('percent_cupom'),
            'flg_ativo' =>  ( Input::has('flg_ativo') ? ( $request->input('flg_ativo') == 1 ? true : false) : false ),
            'cod_usuario_operacao' => $this->usuarioOperacao()
        ]);         

        if($update){
            $this->messagem(config('constants.msg.updateSuccess'));
        }else{
            $this->messagem(config('constants.msg.updateError'));
        }

        return redirect()->route('administracao.cupom.detalhe', [ 'objReturn' => $objReturn ]);
    } 

    public function excluirCupom( String $id )
    {
        $objReturn = $this->cupom->find($id);
        $update = $objReturn->delete();         

        if($update){
            $this->messagem(config('constants.msg.deleteSuccess'));
        }else{
            $this->messagem(config('constants.msg.deleteError'));
        }

        return redirect()->route('administracao.cupom.lista-cupons');
    }  
}
