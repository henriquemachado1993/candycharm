<?php

namespace App\Http\Controllers\Auth;

use App\tp_usuario;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'primeiro_nom_usuario'    => 'required|string|max:100',
            'complemento_nom_usuario'    => 'required|string|max:100',
            'email'      => 'required|string|max:100',            
            'password'      => 'required|string|min:6|confirmed'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return tp_usuario::create([
            'primeiro_nom_usuario'    => $data['primeiro_nom_usuario'],
            'complemento_nom_usuario' => $data['complemento_nom_usuario'],
            'name'             => $data['primeiro_nom_usuario']." ".$data['complemento_nom_usuario'],
            'email'               => $data['email'],
            'password'               => bcrypt($data['password'])
        ]);
    }
}
