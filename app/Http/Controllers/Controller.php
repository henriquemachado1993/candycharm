<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Creitive\Breadcrumbs\Breadcrumbs;
use Illuminate\Support\Facades\Session;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //FUNÇÃO QUE AJUSTA O CAMINHO DAS PAGINAS EXEMPLO: HOME / PRODUTOS / CATEGORIA
    /*FORMA DE USAR
        $objReturn['breadcrumbs'] = $this->breadcrumbs([            
            ['pagina'=>'Nome que irá aparecer no layout', 'rota'=>'rota pra onde sera direcionado']
        ]);
    */
    public function breadcrumbs($data){

        $breadcrumbs = new Breadcrumbs;        
        $breadcrumbs->addCssClasses('breadcrumb'); 
        $breadcrumbs->addCrumb('Home', '/');

        foreach ($data as $key => $value) {        	
        	$breadcrumbs->addCrumb($value['pagina'], $value['rota']);
        }
        
        return $breadcrumbs->render();
    }

    public function messagem($config){        
        Session::flash('message', $config['msg']);
        Session::flash('type', 'alert-'. $config['type']);
    }

    public function messagemCustom($msg, $type){        
        Session::flash('message', $msg);
        Session::flash('type', 'alert-'.$type);
    }

    public function usuarioOperacao()
    {
        return Auth::user()->id;
    }

    public function moedaReal($valor)
    {
        $returnValor = $valor;        
        $returnValor = str_replace(".","", $returnValor);
        $returnValor = str_replace(",",".", $returnValor);

        return (float)$returnValor;
    }
}
