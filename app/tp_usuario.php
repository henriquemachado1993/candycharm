<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class tp_usuario extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'primeiro_nom_usuario',
        'complemento_nom_usuario',
        'name',
        'email',
        'dat_nascimento',
        'num_telefone',
        'num_celular',
        'password'
    ];

    protected $hidden = [
        'password',
        'dsc_token'
    ];

    protected $table = 'tp_usuario';
    protected $primaryKey = 'id';
    protected $softDelete = true;
    protected $dateFormat = 'Y-m-d H:i:s';

    const CREATED_AT = 'dhs_cadastro';
    const UPDATED_AT = 'dhs_atualizacao';
    const DELETED_AT = 'dhs_exclusao_logica';

    //regras para validação de dados
    public $rules_alterar_senha = [
        'password_atual'           => 'required|string',
        'password'                 => 'required|string|min:6|confirmed',  
        'password_confirmation'    => 'required|string|min:6'  
    ];

    //mensagens customizadas
    public $messages_alterar_senha = [
        'password_atual.required'   => 'O campo Senha atual é obrigatório.',
        'password.required'   => 'O campo Nova senha é obrigatório.',
        'password_confirmation.required'  => 'O campo Confirmar nova senha é obrigatória.',
        'password.confirmed'  => 'Confirmação de senha incorreta, preencha novamente os campos.'
       
    ];

}
