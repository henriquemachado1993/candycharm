INSERT INTO public.ta_categoria
(cod_categoria, nom_categoria, dsc_categoria, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('ta_categoria_cod_categoria_seq'::regclass), 'Bolos', 'Categoria para Bolos', now(), now(), null, 0);

INSERT INTO public.ta_categoria
(cod_categoria, nom_categoria, dsc_categoria, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('ta_categoria_cod_categoria_seq'::regclass), 'Tortas', 'Categoria para Tortas', now(), now(), null, 0);

INSERT INTO public.ta_categoria
(cod_categoria, nom_categoria, dsc_categoria, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('ta_categoria_cod_categoria_seq'::regclass), 'Bombons', 'Categoria para Bombons', now(), now(), null, 0);

INSERT INTO public.ta_categoria
(cod_categoria, nom_categoria, dsc_categoria, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('ta_categoria_cod_categoria_seq'::regclass), 'Brigadeiros Gourmet', 'Categoria para Brigadeiros Gourmet', now(), now(), null, 0);

INSERT INTO public.ta_categoria
(cod_categoria, nom_categoria, dsc_categoria, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('ta_categoria_cod_categoria_seq'::regclass), 'Salgados', 'Categoria para Salgados', now(), now(), null, 0);

INSERT INTO public.ta_categoria
(cod_categoria, nom_categoria, dsc_categoria, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('ta_categoria_cod_categoria_seq'::regclass), 'Cupcake', 'Categoria para Cupcake', now(), now(), null, 0);
