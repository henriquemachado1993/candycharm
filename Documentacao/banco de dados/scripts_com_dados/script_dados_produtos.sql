INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 6, 'Bolo de brigadeiro', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 25.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 6, 'Bolo de abacaxi', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 29.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 6, 'Bolo de banana', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 30.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 6, 'Bolo de leite condensado', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 20.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 6, 'Bolo de maçã', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 22.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 6, 'Bolo de canela', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 50.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 6, 'Bolo de chocolate', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 35.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 7, 'Torta de brigadeiro', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 25.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 7, 'Torta de abacaxi', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 29.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 7, 'Torta de banana', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 30.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 7, 'Torta de leite condensado', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 20.99 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 7, 'Torta de maçã', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 22.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 7, 'Torta de canela', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 50.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 7, 'Torta de chocolate', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 35.99 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 8, 'Bombons de brigadeiro', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 15.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 8, 'Bombons de abacaxi', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 11.00 , now(), now(), null, 0);

INSERT INTO public.tp_produto
(cod_produto, cod_categoria, nom_produto, dsc_simples, dsc_completa, vlr_produto, dhs_cadastro, dhs_atualizacao, dhs_exclusao_logica, cod_usuario_operacao)
VALUES(nextval('tp_produto_cod_produto_seq'::regclass), 8, 'Bombons de banana', 'Produto de alta qualidade.', 'Produto de alta qualidade.', 10.00 , now(), now(), null, 0);



