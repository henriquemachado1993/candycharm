drop table ta_categoria;

drop table td_endereco_envio;

drop table td_img;

drop table tp_encomenda;

drop table tp_produto;

drop table tp_usuario;

drop table tr_encomenda_produto;

/*==============================================================*/
/* Table: ta_categoria                                          */
/*==============================================================*/
create table ta_categoria (
   cod_categoria        SERIAL not null,
   nom_categoria        VARCHAR(50)          not null,
   dsc_categoria        TEXT                 null,
   dhs_cadastro         TIMESTAMP            null default CURRENT_TIMESTAMP,
   dhs_atualizacao      TIMESTAMP            null,
   dhs_exclusao_logica  TIMESTAMP            null,
   constraint PK_TA_CATEGORIA primary key (cod_categoria)
);

comment on table td_endereco_envio is
'Tabela para armazenar categoria dos produtos.';

/*==============================================================*/
/* Table: td_endereco_envio                                     */
/*==============================================================*/
create table td_endereco_envio (
   cod_endereco_envio   SERIAL               not null,
   id          INT4                 null,
   dhs_cadastro         TIMESTAMP            null default CURRENT_TIMESTAMP,
   dhs_atualizacao      TIMESTAMP            null,
   dhs_exclusao_logica  TIMESTAMP            null,
   constraint PK_TD_ENDERECO_ENVIO primary key (cod_endereco_envio)
);

comment on table td_endereco_envio is
'Tabela para armazenar local de envio da encomenda.';

/*==============================================================*/
/* Table: td_img                                                */
/*==============================================================*/
create table td_img (
   cod_img              SERIAL not null,
   cod_produto          INT4                 null,
   nom_img              VARCHAR              not null,
   dsc_tamanho_img      VARCHAR              null,
   bol_capa             BOOL                 null,
   dhs_cadastro         TIMESTAMP            null default CURRENT_TIMESTAMP,
   dhs_atualizacao      TIMESTAMP            null,
   dhs_exclusao_logica  TIMESTAMP            null,
   constraint PK_TD_IMG primary key (cod_img)
);

comment on table tp_encomenda is
'Tabela para armazenar encomendas.';

/*==============================================================*/
/* Table: tp_encomenda                                          */
/*==============================================================*/
create table tp_encomenda (
   cod_encomenda        SERIAL not null,
   id          INT4                 null,
   dhs_cadastro         TIMESTAMP            null default CURRENT_TIMESTAMP,
   dhs_atualizacao      TIMESTAMP            null,
   dhs_exclusao_logica  TIMESTAMP            null,
   constraint PK_TP_ENCOMENDA primary key (cod_encomenda)
);

comment on table tp_encomenda is
'Tabela para armazenar encomendas.';

/*==============================================================*/
/* Table: tp_produto                                            */
/*==============================================================*/
create table tp_produto (
   cod_produto          SERIAL not null,
   cod_categoria        INT4                 null,
   nom_produto          VARCHAR              not null,
   dsc_simples          VARCHAR              not null,
   dsc_completa         TEXT                 not null,
   vlr_produto          DECIMAL(7,2)         not null,
   dhs_cadastro         TIMESTAMP            null default CURRENT_TIMESTAMP,
   dhs_atualizacao      TIMESTAMP            null,
   dhs_exclusao_logica  TIMESTAMP            null,
   constraint PK_TP_PRODUTO primary key (cod_produto)
);

comment on table tp_produto is
'Tabela para armazenar produtos.';

/*==============================================================*/
/* Table: tp_usuario                                            */
/*==============================================================*/
create table tp_usuario (
   id          SERIAL not null,
   num_cpf              VARCHAR(15)          null,
   name          VARCHAR(100)         not null,
   primeiro_nom_usuario VARCHAR(50)          not null,
   complemento_nom_usuario VARCHAR(100)         not null,
   email            VARCHAR(100)         not null,
   dat_nascimento       DATE                 null,
   num_telefone         VARCHAR(15)          null,
   num_celular          VARCHAR(15)          null,
   password            VARCHAR(100)         not null,
   dsc_token            VARCHAR              null,
   permissao            VARCHAR(10)          null,
   dhs_cadastro         TIMESTAMP            null default CURRENT_TIMESTAMP,
   dhs_atualizacao      TIMESTAMP            null,
   dhs_exclusao_logica  TIMESTAMP            null,
   constraint PK_TP_USUARIO primary key (id)
);

comment on table tp_usuario is
'Tabela para armazenar usuários.';

/*==============================================================*/
/* Table: tr_encomenda_produto                                  */
/*==============================================================*/
create table tr_encomenda_produto (
   cod_encomenda_produto SERIAL not null,
   cod_encomenda        INT4                 null,
   cod_produto          INT4                 null,
   constraint PK_TR_ENCOMENDA_PRODUTO primary key (cod_encomenda_produto)
);

comment on table tr_encomenda_produto is
'Tabela relacional para armazenar a relação entre produto e encomenda.';

alter table td_endereco_envio
   add constraint FK_TD_ENDER_REFERENCE_TP_USUAR foreign key (id)
      references tp_usuario (id)
      on delete restrict on update restrict;

alter table td_img
   add constraint FK_TD_IMG_REFERENCE_TP_PRODU foreign key (cod_produto)
      references tp_produto (cod_produto)
      on delete restrict on update restrict;

alter table tp_encomenda
   add constraint FK_TP_ENCOM_REFERENCE_TP_USUAR foreign key (id)
      references tp_usuario (id)
      on delete restrict on update restrict;

alter table tp_produto
   add constraint FK_TP_PRODU_REFERENCE_TA_CATEG foreign key (cod_categoria)
      references ta_categoria (cod_categoria)
      on delete restrict on update restrict;

alter table tr_encomenda_produto
   add constraint FK_TR_ENCOM_REFERENCE_TP_ENCOM foreign key (cod_encomenda)
      references tp_encomenda (cod_encomenda)
      on delete restrict on update restrict;

alter table tr_encomenda_produto
   add constraint FK_TR_ENCOM_REFERENCE_TP_PRODU foreign key (cod_produto)
      references tp_produto (cod_produto)
      on delete restrict on update restrict;
