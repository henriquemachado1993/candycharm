<!DOCTYPE html>
<html>
<head>
	<title>Error 404</title>
	<link rel="stylesheet" type="text/css" href="{{ url('/css') }}/bootstrap.min.css" media="screen">
	<style>
		.error-template {padding: 40px 15px;text-align: center;}
		.error-actions {margin-top:15px;margin-bottom:15px;}
		.error-actions .btn { margin-right:10px; }
	</style>
</head>
<body>
	<div class="container">
	    <div class="row">
		    <div class="error-template">
			    <h1>Opa!</h1>
			    <h2>404 não encontrado</h2>
			    <div class="error-details">
					Desculpe, ocorreu um erro, página solicitada não encontrada!<br>			
			    </div>
			    <div class="error-actions">
				<a href="{{ url('/administracao') }}" class="btn btn-primary">
				    <i class="icon-home icon-white"></i> Home </a>				
			    </div>
			</div>
	    </div>
	</div>
</body>
</html>