@extends('master-page')

@section('content')

@include( 'frames.breadcrumbs' )

<div class="container content">

    @include('frames.notificacao')

    <div class="row">
        @include( 'frames.menuEsquerdo' )
        <div class="col-md-9 white-bg">
            <!-- Single Product -->
            <div class="single-product cold-md-12">
                <div class="flexslider col-md-6">
                    <ul class="slides">
                       @foreach( $objReturn['produto']->getImgsProduto as $v )
                            <li data-thumb="{{ url('/imagens/produtos') }}/{{$v->nom_img}}">
                                <img src="{{ url('/imagens/produtos') }}/{{$v->nom_img}}" alt=""/>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="product-details col-md-6">
                    <h1>{{ $objReturn['produto']->nom_produto }}</h1>
                    <p class="price">
                        {{  'R$ '.number_format( $objReturn['produto']->vlr_produto , 2, ',', '.') }}
                    </p>
                    <!-- Quando quiser usar que o preço caiu, só descomentar <span>$27.00</span> -->
                    <p>
                         {{ $objReturn['produto']->dsc_simples }}
                    </p>
                    <form role="form" action="{{ url('/carrinho/adicionar-produto') }}" method="post">
                        {{ csrf_field() }}
                        <div class="qtyminus">
                        </div>
                        <input type="hidden" name="cod_produto" value="{{ $objReturn['produto']->cod_produto }}" class="qty">
                        <input type="text" name="qnt_produto" value="1" class="qty" maxlength="4">
                        <div class="qtyplus">
                        </div>
                        <button type="submit" class="btn btn-success btn-add-carrinho">Adicionar ao carrinho</button>
                    </form>
                    <hr>
                    <div class="availability">
                        <p>
                             Avaliação:
                        </p>
                        <span>Em estoque</span>
                    </div>
                    <div class="reviews">
                        <ul>
                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                            <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                            <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Single Product -->
            </div>
            <div class="col-md-12 tabs">
                <div class="bs-example">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab1" data-loading-text="Loading...">Descrição</a></li>
                            <li><a data-toggle="tab" href="#tab2" data-loading-text="Loading...">Ingredientes</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab1" class="tab-pane active fade in">
                                <p>
                                    {{ $objReturn['produto']->dsc_completa }}
                                </p>
                            </div>
                            <div id="tab2" class="tab-pane fade">
                                <p>
                                     Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus adipiscing. Duis auctor dictum erat hendrerit dapibus.
                                </p>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
            <!--articles-->
            <div class="row articles">
                <div class="col-md-4">
                    <img src="{{ url('/imagens/produtos') }}/product1.jpg" alt="">
                    <div class="text">
                        <a href="#"><span>
                        worldwide shipping </span></a>
                        <p>
                             $ 23.00
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="{{ url('/imagens/produtos') }}/product1.jpg" alt="">
                    <div class="text">
                        <a href="#"><span>
                        worldwide shipping </span></a>
                        <p>
                             $ 23.00
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="{{ url('/imagens/produtos') }}/product1.jpg" alt="">
                    <div class="text">
                        <a href="#"><span>
                        worldwide shipping </span></a>
                        <p>
                             $ 23.00
                        </p>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">                
                $(document).ready(function() {                  
                    $('.flexslider').flexslider({
                        animation: "slide",
                        controlNav: "thumbnails"                       
                    });
                })
            </script>
@endsection