@extends('master-page')

@section('content')
  
@include( 'frames.breadcrumbs' )

<!-- Produtos -->
<div class="container content">
    <div class="row">

        @include( 'frames.menuEsquerdo' )
        @include( 'frames.produtos' )
               
    </div>
</div>
<!-- Fim produtos -->

@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                });
            </script>
@endsection