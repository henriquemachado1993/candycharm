@extends('master-page')

@section('content')
  
@include( 'frames.breadcrumbs' )

<div class="container content">

    @include('frames.notificacao')

    <div class="row">

    	@include( 'frames.sidebarUsuario' )

        <div class="col-md-9">            
            <div class="row bloco">
                <div class="triggerAnimation animated" data-animate="fadeInLeft">                   
                    <div class="title">Alterar senha</div>                       
                </div>
                <form role="form" action="{{ url('/alterar-senha-usuario') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">      
                        <div class="col-md-12 form-group">
                            <label class="alert alert-info">Por questões de segurança, no campo "Senha Atual", digite a sua senha usada atualmente e nos dois campos seguintes a nova senha.</label>                                
                        </div>
                    </div> 
                    <div class="row">      
                        <div class="col-md-4 form-group{{ $errors->has('password_atual') ? ' has-error' : '' }}">
                            <label>SENHA ATUAL</label>
                            <input type="password" class="form-control" name="password_atual">
                            @if ($errors->has('password_atual'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_atual') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col-md-4 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label>NOVA SENHA</label>
                            <input type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-md-4 form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label>CONFIRMAR NOVA SENHA</label>
                            <input type="password" class="form-control" name="password_confirmation">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>                                      
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">                                      
                            <button type="submit" class="btn btn-success">SALVAR</button>
                            <button type="reset" class="btn btn-defalt">LIMPAR</button>
                        </div>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                });
            </script>
@endsection