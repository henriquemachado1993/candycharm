<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>CandyCharm</title>

    <!-- Carregamento dos scripts CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
    <link rel="stylesheet" type="text/css" href="{{ url('/css') }}/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/css') }}/font-awesome.min.css">    

    <link rel="stylesheet" type="text/css" href="{{ url('/layoutCandyCharm') }}/css/styles-form-elements.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/layoutCandyCharm') }}/css/styles-auth.css">           
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="{{ url('/imagens') }}/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{ url('/imagens') }}/favicon.png" type="image/x-icon">
</head>

<body style="background-image: url('{{ url('/layoutCandyCharm/img/candycharm') }}/img_background.jpg');">    
    <!-- Conteudo -->
    @yield( 'content' )

    <!-- Carregamento dos scripts JS -->
    <script src="{{ url('/js') }}/jquery-2.1.1.min.js"></script>
    <script src="{{ url('/js') }}/bootstrap.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <!--[if lt IE 10]>
        <script src="assets/js/placeholder.js"></script>
    <![endif]-->

    @yield( 'dependencyJs' )

</body>
</html>