<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>CandyCharm</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link href="{{ url('/layoutCandyCharmNew') }}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ url('/layoutCandyCharmNew') }}/css/material-kit.css" rel="stylesheet"/>

</head>

<body class="signup-page" style="background-image: url('{{ url('/layoutCandyCharmNew') }}/img/img_background.jpg'); background-size: cover; background-position: top center;">

    <!-- Conteudo -->
    @yield( 'content' )

</body>
    <!--   Core JS Files   -->
    <script src="{{ url('/layoutCandyCharmNew') }}/js/jquery.min.js" type="text/javascript"></script>
    <script src="{{ url('/layoutCandyCharmNew') }}/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{ url('/layoutCandyCharmNew') }}/js/material.min.js"></script>

    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ url('/layoutCandyCharmNew') }}/js/nouislider.min.js" type="text/javascript"></script>

    <!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
    <script src="{{ url('/layoutCandyCharmNew') }}/js/bootstrap-datepicker.js" type="text/javascript"></script>

    <!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
    <script src="{{ url('/layoutCandyCharmNew') }}/js/material-kit.js" type="text/javascript"></script>

    @yield( 'dependencyJs' )
</html>