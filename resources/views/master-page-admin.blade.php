<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>CandyCharm Administracao</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Carregamento dos scripts CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('/css')}}/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/layoutCandyCharmAdmin')}}/vendor/metisMenu/metisMenu.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/layoutCandyCharmAdmin')}}/vendor/datatables-plugins/dataTables.bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/layoutCandyCharmAdmin')}}/dist/css/sb-admin-2.css">
    <link rel="stylesheet" type="text/css" href="{{url('/layoutCandyCharmAdmin')}}/vendor/morrisjs/morris.css">
    <link rel="stylesheet" type="text/css" href="{{url('/layoutCandyCharmAdmin')}}/vendor/font-awesome/css/font-awesome.min.css"/>

    <link rel="stylesheet" type="text/css" href="{{url('/css')}}/styles-flexslider.css" media="screen"/>

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="{{ url('/imagens') }}/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{ url('/imagens') }}/favicon.png" type="image/x-icon">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">CandyCharm Administracao</a>
            </div>
            <ul class="nav navbar-top-links navbar-right">                              
                <li>
                    <a href="{{url('/')}}">
                        <i class="fa fa-user fa-arrow-left"></i> Voltar para CandyCharm
                    </a>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">                       
                        <li>
                            <a href="{{ url('/administracao') }}"><i class="fa fa-dashboard fa-fw"></i> Painel de controle</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i> Catálogo<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                               
                                <li>
                                    <a href="#"><i class="fa fa-shopping-basket fa-fw"></i> Produtos <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{ url('/administracao/produto/lista-produtos') }}"><i class="fa fa-search"></i> Buscar Produto</a>
                                        </li> 
                                        <li>
                                            <a href="{{ url('/administracao/produto/inserir') }}"><i class="fa fa-plus-square"></i> Inserir Produto</a>
                                        </li>                                       
                                    </ul>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-th-list fa-fw"></i> Categoria <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{ url('/administracao/categoria/lista-categorias') }}"><i class="fa fa-search"></i> Buscar Categoria</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/administracao/categoria/inserir') }}"><i class="fa fa-plus-square"></i> Inserir Categoria</a>
                                        </li>                                           
                                    </ul>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-th-large fa-fw"></i> Fornecedores <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{ url('') }}"><i class="fa fa-search"></i> Buscar Fornecedores</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('') }}"><i class="fa fa-plus-square"></i> Inserir Fornecedores</a>
                                        </li>                                           
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-credit-card-alt fa-fw"></i> Pedidos<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                                                     
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-tags fa-fw"></i> Regras de preço<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                               
                                <li>
                                    <a href="#"><i class="fa fa-shopping-basket fa-fw"></i> Cupom <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{ url('/administracao/cupom/lista-cupons') }}"><i class="fa fa-search"></i> Buscar Cupom</a>
                                        </li> 
                                        <li>
                                            <a href="{{ url('/administracao/cupom/inserir') }}"><i class="fa fa-plus-square"></i> Inserir Cupom</a>
                                        </li>                                       
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-truck fa-fw"></i> Envio<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                                                     
                            </ul>
                        </li>                                 
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Conteúdo principal --> 
        @yield('pageheader')
        @yield('content-admin')
        <!-- Fim conteúdo principal -->

    </div>
  
    <!-- scripts jQuery -->
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/vendor/metisMenu/metisMenu.min.js"></script>
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/vendor/raphael/raphael.min.js"></script>
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/vendor/morrisjs/morris.min.js"></script>
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/data/morris-data.js"></script>
    <!-- DataTables JavaScript -->
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/vendor/datatables-responsive/dataTables.responsive.js"></script>    
    <!-- Scripts específicos da area administrativa -->
    <script type="text/javascript" src="{{url('/layoutCandyCharmAdmin')}}/dist/js/sb-admin-2.js"></script>
    <!-- Scripts para mascaras de input -->
    <script type="text/javascript" src="{{url('/js')}}/jquery.maskMoney.min.js"></script>
    <script type="text/javascript" src="{{url('/js')}}/jquery.maskedinput.min.js"></script>
    <!-- Slider JavaScript -->
    <script type="text/javascript" src="{{url('/js')}}/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="{{url('/js')}}/genericScripts.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
          
        });
    </script>
    @yield( 'dependencyJs' )

</body>
</html>
