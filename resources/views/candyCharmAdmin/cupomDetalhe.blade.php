@extends('master-page-admin')

@section('content-admin')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detalhe Cupom</h1>
                </div> 
            </div>
            
            @include('frames.notificacao')

            @if(!is_null($objReturn))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Cupom
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">                                    
                                        <p><strong>Nome do cupom:</strong> {{ $objReturn->nom_cupom }} </p>
                                        <p><strong>Desconto: </strong> {{ $objReturn->percent_cupom }}% </p> 
                                        <p><strong>Ativo:</strong> {{ ( $objReturn->flg_ativo ? 'Sim' : 'Não') }} </p>
                                        <p><strong>Data de cadastro:</strong> {{ Carbon\Carbon::parse( $objReturn->dhs_cadastro )->format('d/m/Y H:i:s') }} </p>
                                        <p><strong>Data da última alteração:</strong> {{ Carbon\Carbon::parse( $objReturn->dhs_atualizacao )->format('d/m/Y H:i:s') }}  </p>
                                        <a href="{{ url('/administracao/cupom/alterar') }}/{{ $objReturn->cod_cupom }}" class="btn btn-success">Alterar</a>
                                        <a href="{{url('/administracao/cupom/inserir')}}" class="btn btn-info">Inserir novo</a>    
                                        <a href="{{ url('/administracao/cupom/excluir-cupom') }}/{{ $objReturn->cod_cupom }}" class="btn btn-danger">Excluir</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
               
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
			
            <script type="text/javascript">
                $(document).ready(function() {
                	   	
                });
            </script>
@endsection