@extends('master-page-admin')

@section('content-admin')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lista de Categorias</h1>
                </div>
            </div>

            @include('frames.notificacao')
            <div class="row">
                <div class="col-lg-12">
                   <a href="{{url('/administracao/categoria/inserir')}}" class="btn btn-info">Inserir novo</a>  
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tabela categoria
                        </div>
                        <div class="panel-body">
                            <table id="dtCategoria" width="100%" class="table table-striped table-bordered table-hover">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
			 
            <script type="text/javascript">
                
                var dtColumnsCategoria = function() {
                    var columns = [
                        {"sTitle": "ID","width": "10%", "sName": "cod_categoria", "mData": "cod_categoria"},
                        {"sTitle": "Nome Categoria", "width": "25%", "sName": "nom_categoria", "mData": "nom_categoria"},
                        {"sTitle": "Desc. Categoria", "sName": "dsc_categoria", "mData": "dsc_categoria"},
                        {"sTitle": "", "width": "155px", "searchable": false, "orderable":  false, "data": function( data){

                                var button  = '<a title="Alterar" href="/administracao/categoria/alterar/'+ data.cod_categoria +'" class="btn btn-success mgn-btn-dt"><i class="fa fa-pencil-square-o"></i></a>';
                                button  += ' <a title="Detalhes" href="/administracao/categoria/detalhe/'+ data.cod_categoria +'" class="btn btn-info mgn-btn-dt"><i class="fa fa-file-text-o"></i></a>';
                                button  += ' <a title="Excluir" href="/administracao/categoria/excluir-categoria/'+ data.cod_categoria +'" class="btn btn-danger mgn-btn-dt"><i class="fa fa-trash"></i></a>';
                                return button;
                            }
                        }
                    ]

                    return columns;
                }

                $(document).ready(function() {
                    dtTable({ 
                            id_tabela : 'dtCategoria',
                            url_data : '/administracao/categoria/lista-categorias.json',
                            colunas: dtColumnsCategoria
                    });  	
                });

            </script>
@endsection