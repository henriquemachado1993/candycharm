@extends('master-page-admin')

@section('content-admin')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Alterar Cupom</h1>
                </div>
            </div>

            @include('frames.notificacao')
            
            @if(!is_null($objReturn))
                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Cupom
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form role="form" action="{{ url('/administracao/cupom/alterar-cupom') }}" method="post">
                                            {{ csrf_field() }}
                                           <input type="hidden" class="form-control" name="id" value="{{ $objReturn->cod_cupom }}">                         
                                            <div class="form-group{{ $errors->has('nom_cupom') ? ' has-error' : '' }}">
                                                <label>*Nome do cupom</label>
                                                <input class="form-control" placeholder="Nome" name="nom_cupom" value="{{ $objReturn->nom_cupom }}">
                                               
                                                @if ($errors->has('nom_cupom'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('nom_cupom') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('percent_cupom') ? ' has-error' : '' }}">
                                                <label>*Desconto %</label>
                                                <select name="percent_cupom" class="form-control">
                                                    <option value="{{$objReturn->percent_cupom}}">{{$objReturn->percent_cupom}}</option>
                                                    @for ($i = 1; $i < 100; $i++)
                                                       <option value="{{ $i }}">{{ $i }}</option>         
                                                    @endfor                                                 
                                                </select>
                                                @if ($errors->has('percent_cupom'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('percent_cupom') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label><input type="checkbox" class="" name="flg_ativo" value="1" {{( $objReturn->flg_ativo ? "checked": "")}}> Ativo</label>
                                            </div>                           
                                            <button type="submit" class="btn btn-success">Salvar</button>
                                            <button type="reset" class="btn btn-default">Limpar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
			
            <script type="text/javascript">
                $(document).ready(function() {
                	   	
                });
            </script>
@endsection