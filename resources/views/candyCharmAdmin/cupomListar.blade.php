@extends('master-page-admin')

@section('content-admin')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lista de Cupons</h1>
                </div>
            </div>

            @include('frames.notificacao')
            <div class="row">
                <div class="col-lg-12">
                   <a href="{{url('/administracao/cupom/inserir')}}" class="btn btn-info">Inserir novo</a>  
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tabela cupons
                        </div>
                        <div class="panel-body">
                            <table id="dtCupons" width="100%" class="table table-striped table-bordered table-hover">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
			 
            <script type="text/javascript">
                
                var dtColumnsCupons = function() {
                    var columns = [
                        {"sTitle": "ID","width": "10%", "sName": "cod_cupom", "mData": "cod_cupom"},
                        {"sTitle": "Nome Cupom", "width": "25%", "sName": "nom_cupom", "mData": "nom_cupom"},
                        {"sTitle": "Desconto", "sName": "percent_cupom", "mData": function( data){
                                return data.percent_cupom + "%";
                            }
                        },
                        {"sTitle": "Ativo", "sName": "flg_ativo", "mData": function( data){
                                return ( data.flg_ativo ? "Sim" : "Não");
                            }
                        },
                        {"sTitle": "", "width": "155px", "searchable": false, "orderable":  false, "data": function( data){

                                var button  = '<a title="Alterar" href="/administracao/cupom/alterar/'+ data.cod_cupom +'" class="btn btn-success mgn-btn-dt"><i class="fa fa-pencil-square-o"></i></a>';
                                button  += ' <a title="Detalhes" href="/administracao/cupom/detalhe/'+ data.cod_cupom +'" class="btn btn-info mgn-btn-dt"><i class="fa fa-file-text-o"></i></a>';
                                button  += ' <a title="Excluir" href="/administracao/cupom/excluir-cupom/'+ data.cod_cupom +'" class="btn btn-danger mgn-btn-dt"><i class="fa fa-trash"></i></a>';
                                return button;
                            }
                        }
                    ]

                    return columns;
                }

                $(document).ready(function() {
                    dtTable({ 
                            id_tabela : 'dtCupons',
                            url_data : '/administracao/cupom/lista-cupons.json',
                            colunas: dtColumnsCupons
                    });  	
                });

            </script>
@endsection