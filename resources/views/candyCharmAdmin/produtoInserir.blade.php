@extends('master-page-admin')

@section('content-admin')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Inserir Produto</h1>
                </div>
            </div>

            @include('frames.notificacao')

            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Categoria
                        </div>
                        <div class="panel-body">
                            <form role="form" action="{{ url('/administracao/produto/inserir-produto') }}" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        {{ csrf_field() }}                         
                                        <div class="form-group{{ $errors->has('nom_produto') ? ' has-error' : '' }}">
                                            <label>*Nome do produto</label>
                                            <input class="form-control" placeholder="Nome do produto" name="nom_produto" value="{{ old('nom_produto') }}">
                                            @if ($errors->has('nom_produto'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('nom_produto') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('dsc_simples') ? ' has-error' : '' }}">
                                            <label>*Descrição Simples</label>
                                            <textarea class="form-control" placeholder="Descrição Simples" rows="3" name="dsc_simples" >{{ old('dsc_simples') }}</textarea>
                                            @if ($errors->has('dsc_simples'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('dsc_simples') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('dsc_completa') ? ' has-error' : '' }}">
                                            <label>*Descrição Completa</label>
                                            <textarea class="form-control" placeholder="Descrição Completa" rows="3" name="dsc_completa" >{{ old('dsc_completa') }}</textarea>
                                            @if ($errors->has('dsc_completa'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('dsc_completa') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('cod_categoria') ? ' has-error' : '' }}">
                                            <label>*Categoria</label>
                                            <select name="cod_categoria" class="form-control">
                                                <option value="" selected="" disabled="">Selecione...</option>
                                                @foreach( $objReturn['categoria'] as $v )
                                                    <option value="{{$v->cod_categoria}}">{{$v->nom_categoria}}</option>
                                                @endforeach                                            
                                            </select>

                                            @if ($errors->has('cod_categoria'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('cod_categoria') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('vlr_produto') ? ' has-error' : '' }}">
                                            <label>*Valor</label>
                                            <input id="vlr_produto" class="form-control" placeholder="R$" name="vlr_produto" value="{{ old('vlr_produto') }}">
                                            @if ($errors->has('vlr_produto'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('vlr_produto') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label><input type="checkbox" class="" name="flg_oferta" value="s"> Marcar como oferta</label>
                                        </div>

                                        <div class="form-group{{ count($errors) > 0 ? ' has-error' : '' }}">
                                            <label>*Adicionar imagens</label>
                                            <input type="file" class="form-control" name="nom_imagem[]" multiple> 
                                            @if (isset($errors) && count($errors) > 0 )
                                                <span class="help-block">
                                                    @foreach($errors->all() as $error)
                                                        <strong> {{$error}} </strong><br>
                                                    @endforeach                                                    
                                                </span>
                                            @endif                                             
                                        </div>     
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-success">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
            
            <script type="text/javascript">
                $(document).ready(function() {
                    
                    //Inclui a mascara no campo de valor
                    $(function(){
                      $("#vlr_produto").maskMoney({thousands:'.',decimal:','});
                    });
                                        
                });
            </script>
@endsection