@extends('master-page-admin')

@section('content-admin')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detalhe Categoria</h1>
                </div> 
            </div>
            
            @include('frames.notificacao')

            @if(!is_null($objReturn))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Categoria
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">                                    
                                        <p><strong>Nome da categoria:</strong> {{ $objReturn->nom_categoria }} </p>
                                        <p><strong>Descrição da categoria:</strong> {{ $objReturn->dsc_categoria }} </p>
                                        <p><strong>Data de cadastro:</strong> {{ Carbon\Carbon::parse( $objReturn->dhs_cadastro )->format('d/m/Y H:i:s') }} </p>
                                        <p><strong>Data da última alteração:</strong> {{ Carbon\Carbon::parse( $objReturn->dhs_atualizacao )->format('d/m/Y H:i:s') }}  </p>
                                        <a href="{{ url('/administracao/categoria/alterar') }}/{{ $objReturn->cod_categoria }}" class="btn btn-success">Alterar</a>
                                        <a href="{{url('/administracao/categoria/inserir')}}" class="btn btn-info">Inserir novo</a>    
                                        <a href="{{ url('/administracao/categoria/excluir-categoria') }}/{{ $objReturn->cod_categoria }}" class="btn btn-danger">Excluir</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
               
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
			
            <script type="text/javascript">
                $(document).ready(function() {
                	   	
                });
            </script>
@endsection