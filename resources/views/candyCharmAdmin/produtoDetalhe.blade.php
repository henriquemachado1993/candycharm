@extends('master-page-admin')

@section('content-admin')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detalhes do Produto</h1>
                </div>
            </div>

            @include('frames.notificacao')

            @if(!is_null($objReturn))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Produto
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-7">                                    
                                        <p><strong>Nome do produto:</strong> {{ $objReturn->nom_produto }} </p>
                                        <p><strong>Descrição simples:</strong> {{ $objReturn->dsc_simples }} </p>
                                        <p><strong>Descrição completa:</strong> {{ $objReturn->dsc_completa }} </p>
                                        <p><strong>Categoria:</strong> {{ $objReturn->getCategorias->nom_categoria}} </p>
                                        <p><strong>Valor: </strong>{{  'R$ '.number_format($objReturn->vlr_produto, 2, ',', '.') }}</p>
                                        <p><strong>Oferta: </strong>{{ $objReturn->flg_oferta }}</p>
                                        <p><strong>Data de cadastro: </strong>{{ Carbon\Carbon::parse( $objReturn->dhs_atualizacao )->format('d/m/Y H:i:s') }}</p>
                                        <p><strong>Data de atualização: </strong>{{ Carbon\Carbon::parse( $objReturn->dhs_atualizacao )->format('d/m/Y H:i:s') }} </p>
                                    </div>
                                    <div class="col-lg-5">
                                        <p><strong>Imagens</strong></p>
                                        <div class="flexslider">
                                            <ul class="slides">
                                                @foreach( $objReturn->getImgsProduto as $v )
                                                    <li data-thumb="{{ url('/imagens/produtos') }}/{{$v->nom_img}}">
                                                        <img style="width:250; height:300px;" src="{{ url('/imagens/produtos') }}/{{$v->nom_img}}" alt=""/>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">    
                                        <a href="" class="btn btn-success">Alterar</a>
                                        <a href="{{url('/administracao/produto/inserir')}}" class="btn btn-info">Inserir novo</a>    
                                        <a href="" class="btn btn-danger">Excluir</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
@endsection

@section( 'dependencyJs' )
			
            <script type="text/javascript">
                $(document).ready(function() {
            	   	$('.flexslider').flexslider({
                        animation: "slide",
                        controlNav: "thumbnails"                       
                    });
                });
            </script>
@endsection