@extends('master-page-admin')

@section('content-admin')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lista de Produtos</h1>
                </div>
            </div>

            @include('frames.notificacao')
            <div class="row">
                <div class="col-lg-12">
                   <a href="{{url('/administracao/produto/inserir')}}" class="btn btn-info">Inserir novo</a>  
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tabela produto
                        </div>
                        <div class="panel-body">
                            <table id="dtProduto" width="100%" class="table table-striped table-bordered table-hover">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
             
            <script type="text/javascript">
                
                var dtColumnsProduto = function() {
                    var columns = [
                        {"sTitle": "Img","width": "60px", "sName": "cod_categoria", "mData": function(data) {                                
                                var img = "";
                                if(data.get_imgs_produto.length > 0){
                                    img = '<div><img style="width:80px; height:80px;" src="{{ url('/imagens/produtos') }}/'+ data.get_imgs_produto[0].nom_img +'" alt=""></div>';
                                }

                                return img;
                            }
                        },
                        {"sTitle": "Nome Produto", "width": "25%", "sName": "nom_categoria", "mData": "nom_produto"},                 
                        {"sTitle": "Desc. simples", "sName": "dsc_simples", "mData": "dsc_simples"},
                        {"sTitle": "Oferta", "width": "5%", "sName": "flg_oferta", "mData": function(data){
                                var retorno = "";
                                if (IsNotNullOrEmpty(data.flg_oferta)) {
                                    if(data.flg_oferta == "s"){
                                        retorno = "Sim";
                                    }else if(data.flg_oferta == "n"){
                                        retorno = "Não";
                                    }
                                }
                                return retorno;
                            }
                        }, 
                        {"sTitle": "Valor", "sName": "vlr_produto", "mData": function(data){
                                return "R$ "+ data.vlr_produto;
                            }
                        }, 
                        {"sTitle": "", "width": "155px", "searchable": false, "orderable":  false, "data": function(data){

                                var button  = '<a title="Alterar" href="/administracao/produto/alterar/'+ data.cod_produto +'" class="btn btn-success mgn-btn-dt"><i class="fa fa-pencil-square-o"></i></a>';
                                button  += ' <a title="Detalhes" href="/administracao/produto/detalhe/'+ data.cod_produto +'" class="btn btn-info mgn-btn-dt"><i class="fa fa-file-text-o"></i></a>';
                                button  += ' <a title="Excluir" href="/administracao/produto/excluir-produto/'+ data.cod_produto +'" class="btn btn-danger mgn-btn-dt"><i class="fa fa-trash"></i></a>';
                                return button;
                            }
                        }                        
                    ]

                    return columns;
                }

                $(document).ready(function() {
                    dtTable({ 
                            id_tabela : 'dtProduto',
                            url_data : '/administracao/produto/lista-produtos.json',
                            colunas: dtColumnsProduto
                    });     
                });

            </script>
@endsection