@extends('master-page-admin')

@section('content-admin')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Inserir Categoria</h1>
                </div>
            </div>

            @include('frames.notificacao')

            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Categoria
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="{{ url('/administracao/categoria/inserir-categoria') }}" method="post">
                                        {{ csrf_field() }}                         
                                        <div class="form-group{{ $errors->has('nom_categoria') ? ' has-error' : '' }}">
                                            <label>*Nome da categoria</label>
                                            <input class="form-control" placeholder="Nome" name="nom_categoria" value="{{ old('nom_categoria') }}">
                                            @if ($errors->has('nom_categoria'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('nom_categoria') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('dsc_categoria') ? ' has-error' : '' }}">
                                            <label>*Descrição da categoria</label>
                                            <textarea class="form-control" placeholder="Descrição" rows="3" name="dsc_categoria" value="{{ old('dsc_categoria') }}"></textarea>
                                            @if ($errors->has('dsc_categoria'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('dsc_categoria') }}</strong>
                                                </span>
                                            @endif
                                        </div>                                    
                                        <button type="submit" class="btn btn-success">Salvar</button>
                                        <button type="reset" class="btn btn-default">Limpar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
			
            <script type="text/javascript">
                $(document).ready(function() {
                	   	
                });
            </script>
@endsection