@extends('master-page')

@section('content')
  
@include( 'frames.breadcrumbs' )

<div class="container content">

    @include('frames.notificacao')

    <div class="row">
    	 <div class="col-md-3 shop-sidebar">
            <div class="sidebar-widgets">
                <!-- <div class="row right-cal">
                    <h4>opening time</h4>
                    <ul>
                        <li><a href="#">monday<span>8am-5pm</span></a></li>
                        <li class="colored"><a href="#">tuesday<span>8am-5pm</span></a></li>
                        <li><a href="#">wednesday<span>8am-5pm</span></a></li>
                        <li class="colored"><a href="#">thursday<span>8am-5pm</span></a></li>
                        <li><a href="#">friday<span>8am-5pm</span></a></li>
                        <li class="colored"><a href="#">saturday<span>8am-5pm</span></a></li>
                        <li><a href="#">sunday<span>8am-5pm</span></a></li>
                    </ul>
                </div> -->
                <div class="row right-inf">
                    <h4>Contato</h4>
                    <ul>
                        <li>
                        <p>
                             CandyCharm Webshop
                        </p>
                        </li>
                        <li>
                        <p>
                             Rua 522 bairro nobre, Brasilia - DF , Brasil
                        </p>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#"><i class="fa fa-phone"></i> (61) 3358-2256</a></li>
                        <li><a href="#"><i class="fa fa-phone"></i> (61) 3358-2256</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">            
            <div class="row contact-all">
                <div class="triggerAnimation animated" data-animate="fadeInLeft">
                    <form id="contact-form" action="{{ url('/sobre-nos/enviar-email') }}" method="post">
                        {{ csrf_field() }}
                        <h1>Contate-nos</h1>
                        <div class="text-fields">
                            <div class="float-input">
                                <input name="nome" id="name" type="text" placeholder="Seu nome">
                                <span><i class="fa fa-user"></i></span>
                            </div>
                            <div class="float-input">
                                <input name="email" id="mail" type="text" placeholder="E-mail">
                                <span><i class="fa fa-envelope-o"></i></span>
                            </div>                          
                        </div>
                        <div class="submit-area">
                            <textarea name="mensagem" id="comment" placeholder="Mensagem"></textarea>
                            <input type="submit" id="submit_contact" class="main-button" value="Enviar">
                            <div id="msg" class="message">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section( 'dependencyJs' )
			
            <script type="text/javascript">
                $(document).ready(function() {
                	   	
                });
            </script>
@endsection