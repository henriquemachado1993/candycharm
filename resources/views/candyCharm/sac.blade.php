@extends('master-page')

@section('content')
  
@include( 'frames.breadcrumbs' )

<div class="container content">

    @include('frames.notificacao')

    <div class="row">

        @include( 'frames.sidebarUsuario' )

        <div class="col-md-9">            
            <div class="row bloco">
                <div class="triggerAnimation animated" data-animate="fadeInLeft">                   
                    <div class="title">Dados Cadastrais</div>                       
                </div>

                <form role="form" action="{{ url('/sac/enviar-email') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">      
                        <div class="col-md-12 form-group">
                            <label class="alert alert-info">Através deste serviço, você poderá tirar dúvidas à respeito de produtos, prazos, formas de pagamento, serviço de entrega e outros assuntos. Estamos prontos para atendê-lo de forma fácil e rápida.</label>                                
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col-md-7 form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                            <label>SEU NOME</label>
                            <input type="text" class="form-control" name="nome" value="{{ old('nome') }}">
                            @if ($errors->has('nome'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nome') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col-md-7 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>E-MAIL</label>
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col-md-4 form-group">
                            <label>TELEFONE</label>
                            <input type="text" class="form-control" name="telefone" value="{{ old('telefone') }}">
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col-md-4 form-group">
                            <label>CELULAR</label>
                            <input type="text" class="form-control" name="celular" value="{{ old('celular') }}">
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col-md-12 form-group{{ $errors->has('mensagem') ? ' has-error' : '' }}">
                            <label>MENSAGEM</label>
                            <textarea name="mensagem" class="form-control" rows="6">{{old('mensagem')}}</textarea>
                            @if ($errors->has('mensagem'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('mensagem') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">                                      
                            <button type="submit" class="btn btn-primary">ENVIAR</button>
                        </div>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                    jQuery(function($){
                       $("input[name='telefone']").mask("(99) 9999-99999");
                       $("input[name='celular']").mask("(99) 9999-99999");
                    });
                });
            </script>
@endsection