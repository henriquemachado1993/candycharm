@extends('master-page')

@section('content')
  
     @include( 'frames.bannerSlides' )

    <!--Icon Blocks-->
    <div class="container">
        <div class="icon-blocks">
            <div class="col-md-4">
                <p>
                    <i class="fa fa-group"></i>personalized shop
                </p>
            </div>
            <div class="col-md-4">
                <p>
                    <i class="fa fa-bell"></i>email notification
                </p>
            </div>
            <div class="col-md-4">
                <p>
                    <i class="fa fa-globe"></i>worldwide shipping
                </p>
            </div>
        </div>
    </div>
    <!--end Icon Blocks-->

    <!-- Produtos -->
    @include( 'frames.produtosOfertas' )
    <!-- Fim produtos -->    
@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                    
                    //Inicia slider banner
                    $('.main-flexslider').flexslider({
                        directionNav: true, 
                        controlNav: false, 
                        animation: "fade",
                        slideshowSpeed: 7000,
                        prevText: "",
                        nextText: "",
                    });
                });
            </script>
@endsection