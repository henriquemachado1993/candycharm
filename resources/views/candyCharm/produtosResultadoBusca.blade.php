@extends('master-page')

@section('content')
  
@include( 'frames.breadcrumbs' )

<!-- Produtos -->
<div class="container content">
	<div class="row">
		<div class="col-md-4 result-busca">
			<label>{{ count($objReturn['produto']) }} resultado(s)</label>
		</div>
		<div class="col-md-2 col-md-offset-6 result-busca">
			<select name="cod_categoria" class="form-control">
	            <option value="" selected="" disabled="">Ordernar por</option>
	            <option value="">Menor preço</option>
	            <option value="">Maior preço</option>
	            <option value="">A-Z</option>
	            <option value="">Z-A</option>
	        </select>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12 shop-section">
		    <div class="row articles">
	    		@if( count($objReturn['produto']) > 0 )
        			@foreach( $objReturn['produto'] as $v )
		                <div class="col-md-3 col-sm-5">
		                    <div class="product">
		                        <!-- TODO: arrumar a questão para mostrar imagem que vem do banco -->
		                        <img src="{{ url('/imagens/produtos') }}/product1.jpg" alt="">
		                        <div class="text">
		                            <a class="link-product" title="{{ $v->nom_produto }}" href="{{ url('/produto') }}/{{$v->cod_produto}}">
		                                <span>
		                                	{{ $v->nom_produto }}
		                                </span>                              
		                                <p class="vlr-produto"> {{  'R$ '.number_format($v->vlr_produto, 2, ',', '.') }}</p>
		                            </a>
		                        </div>
		                    </div>
		                </div>
	               @endforeach
		        @else
		            <div class="col-md-12">
		                <p>
		                	Nenhum produto encontrado
		                </p>
		            </div>      
		        @endif
		    </div>    
		    <div class="row">
		        <div class="col-md-12 latest"> 
		    		{{ $objReturn['produto']->links() }}
		        </div>
		    </div>
		</div>            
    </div>
</div>
<!-- Fim produtos -->

@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                });
            </script>
@endsection