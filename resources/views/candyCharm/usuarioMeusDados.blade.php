@extends('master-page')

@section('content')
  
@include( 'frames.breadcrumbs' )

<div class="container content">

    @include('frames.notificacao')

    <div class="row">

    	@include( 'frames.sidebarUsuario' )

        <div class="col-md-9">            
            <div class="row bloco">
                <div class="triggerAnimation animated" data-animate="fadeInLeft">                   
                    <div class="title">Dados Cadastrais</div>                       
                </div>

                <form role="form" action="{{ url('/administracao/categoria/inserir-categoria') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">      
                        <div class="col-md-12 form-group">
                            <label class="alert alert-info">Todos os campos são de preenchimento obrigatório.</label>                                
                        </div>
                    </div>
                     <div class="box-texto">Dados Pessoais</div>
                    <div class="row">      
                        <div class="col-md-4 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label>PRIMEIRO NOME</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                         <div class="col-md-4 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label>ÚLTIMO NOME</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col-md-4 form-group{{ $errors->has('num_cpf') ? ' has-error' : '' }}">
                            <label>CPF</label>
                            <input type="text" class="form-control" name="num_cpf" value="{{ old('num_cpf') }}">
                            @if ($errors->has('num_cpf'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('num_cpf') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col-md-4 form-group{{ $errors->has('dat_nascimento') ? ' has-error' : '' }}">
                            <label>DATA DE NASCIMENTO</label>
                            <input type="text" class="form-control" name="dat_nascimento" value="{{ old('dat_nascimento') }}">
                            @if ($errors->has('dat_nascimento'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('dat_nascimento') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                   	 <div class="box-texto">Contato</div>
                    <div class="row">      
                        <div class="col-md-4 form-group{{ $errors->has('num_telefone') ? ' has-error' : '' }}">
                            <label>TELEFONE</label>
                            <input type="text" class="form-control" name="num_telefone" value="{{ old('num_telefone') }}">
                            @if ($errors->has('num_cpf'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('num_telefone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col-md-4 form-group{{ $errors->has('num_celular') ? ' has-error' : '' }}">
                            <label>CELULAR</label>
                            <input type="text" class="form-control" name="num_celular" value="{{ old('num_celular') }}">
                            @if ($errors->has('num_celular'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('num_celular') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">                                      
                            <button type="submit" class="btn btn-success">ALTERAR</button>
                            <button type="reset" class="btn btn-defalt">LIMPAR</button>
                        </div>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                    jQuery(function($){
                       $("input[name='dat_nascimento']").mask("99/99/9999");
                       $("input[name='num_cpf']").mask("999.999.999-99");
                       $("input[name='num_telefone']").mask("(99) 9999-99999");
                       $("input[name='num_celular']").mask("(99) 9999-99999");
                    });
                });
            </script>
@endsection