@extends('master-page')

@section('content')

@include( 'frames.breadcrumbs' )

<!-- shop-page -->
<div class="container content">

    @include('frames.notificacao')

    <div class="row">
        <div class="col-md-12">
            <div class="wizard bloco">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-texto">CARRINHO DE COMPRAS</div>
                    </div>
                </div>
                <div class="row mb-15-px">
                    <div class="col-md-12">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs-wizard" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Carrinho">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-shopping-cart"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Identificação">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-user"></i>
                                        </span>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Confirmação">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-credit-card"></i>
                                        </span>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Concluído">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-ok"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                         <a href="{{ url('/produtos') }}" class="btn btn-warning "><span class="glyphicon glyphicon-arrow-left"></span> Escolher mais produtos</a></li>
                    </div>
                </div>
                <form role="form">
                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table cart-table responsive-table">
                                        <tr>
                                            <th>Item</th>
                                            <th>Produtos</th>
                                            <th>Quantidade</th>
                                            <th>Preço</th>   
                                            <th></th>
                                        </tr>
                                        <tr style="display: none;"><form></form></tr>
                                        @if( count($objReturn['produtosCarrinho']) > 0 )
                                            @foreach( $objReturn['produtosCarrinho'] as $v )
                                               <tr>
                                                    <td>
                                                        <img src="{{ url('/imagens/produtos') }}/product2-small.jpg" alt=""/>
                                                    </td>
                                                    <td class="cart-title">
                                                        <a href="{{url('/produto')}}/{{$v['id']}}">{{ $v['name'] }}</a>
                                                    </td>
                                                    <td>
                                                        <form role="form" action='{{ url('/carrinho/altera-qnt-produto') }}' method="post">
                                                            {{ csrf_field() }}
                                                            <input type='hidden' name="cod_produto" value='{{$v['id']}}' class="qty"/>
                                                            <input type='text' name="qnt_produto" value='{{$v['quantity']}}' class="qty" maxlength="4"/>
                                                            <button type='submit' title="Atualizar Carrinho" class="ml-3-px cart-remove btn btn-success" ><i class="fa fa-refresh"></i></button>
                                                        </form>                                               
                                                    </td>
                                                    <td>
                                                         {{  'R$ '.number_format($v['price'], 2, ',', '.') }}
                                                    </td>
                                                    <td>
                                                        <a href="{{ url('/carrinho/remover-produto') }}/{{$v['id']}}" title="Remover do Carrinho" class="pull-right cart-remove btn btn-danger" ><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7"> SEU CARRINHO ESTÁ VAZIO </td>
                                            </tr>   
                                        @endif   

                                    </table>
                                </div>
                            </div>   
                            
                            @if( count($objReturn['produtosCarrinho']) > 0 )
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{ url('/carrinho/limpar') }}" class="btn btn-danger "><span class="fa fa-trash"></span> Limpar Carrinho</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="cart-totals">
                                                    <table class="table cart-table test">
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    CUPOM DE DESCONTO
                                                                </th>
                                                                <td> 
                                                                    <form role="form" action='{{ url('/carrinho/aplica-cupom') }}' method="post">
                                                                        {{ csrf_field() }}
                                                                        <div class="row">
                                                                            <div class="col-md-9 {{ $errors->has('nom_cupom') ? ' has-error' : '' }}">
                                                                                <input type='text' name="nom_cupom" class="form-control" placeholder="Digite seu cupom aqui" />
                                                                                @if ($errors->has('nom_cupom'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('nom_cupom') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <button type="submit" class="btn btn-primary next-step pull-right"><span class="glyphicon glyphicon-ok"></span> Aplicar</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="cart-totals">
                                                    <table class="table cart-table test">
                                                        <tbody>
                                                            <tr>
                                                                <th>
                                                                    CALCULAR ENTREGA
                                                                </th>
                                                                <td> 
                                                                    <form role="form">
                                                                        {{ csrf_field() }}
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                 <select name="dhs_frete" class="form-control">
                                                                                    <option value="" selected="" disabled="">Selecione...</option>
                                                                                    <option value="retirar_loja">Retirar na loja</option>      
                                                                                    <option value="a_combinar">A combinar</option>      
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col-md-6">
                                        <div class="cart-totals">
                                            <table class="table cart-table test">
                                                <tbody>
                                                    <tr>
                                                        <th>
                                                            Total com descontos
                                                        </th>
                                                        <td>
                                                            <strong>{{'R$ '.number_format($objReturn['vlrTotalCarrinho'], 2, ',', '.') }}</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>
                                                            Frete
                                                        </th>
                                                        <td>
                                                            R$ 0,00
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>
                                                             Valor Total
                                                        </th>
                                                        <td>
                                                            <strong>{{'R$ '.number_format($objReturn['vlrTotalCarrinho'], 2, ',', '.') }}</strong>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-primary next-step pull-right"><span class="glyphicon glyphicon-ok"></span> Finalizar pedido</button>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="tab-pane" role="tabpanel" id="step2">
                            <div class="box-texto">IDENTIFICAÇÃO DO USUÁRIO</div>
                            
                            @if (Auth::check())
                                   

                                <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                           <h4>Logado</h4> 
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            @else
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-5 col-md-offset-3">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3>DADOS PESSOAIS  <i class="fa fa-user"></i></h3>
                                                        <p>E-mail e Senha obrigatórios para o login</p>
                                                    </div>
                                                </div><br/>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <form role="form" action="{{ route('login') }}" method="post" class="login-form smart-form">
                                                            {{ csrf_field() }}
                                                            
                                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                                <label class="sr-only" for="email">E-mail</label>
                                                                <input type="email" name="email" placeholder="E-mail" class="form-username form-control" id="email" value="{{ old('email') }}" required autofocus>
                                                                @if ($errors->has('email'))
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('email') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>

                                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                                <label class="sr-only" for="password">Senha</label>
                                                                <input type="password" name="password" placeholder="Senha" class="form-password form-control" id="password" required>
                                                                @if ($errors->has('password'))
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('password') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                
                                                                    <label>
                                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Manter a sessão aberta
                                                                    </label>
                                                            </div>
                                                            <div class="form-group">
                                                                    <button type="submit" class="btn">
                                                                        Entrar
                                                                    </button>
                                                                    <a class="btn btn-link" href="{{ route('register') }}">
                                                                        Registrar-se
                                                                    </a>
                                                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                                                        Esqueceu sua senha?
                                                                    </a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>                   
                                        </div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary next-step pull-left"><span class="glyphicon glyphicon-ok"></span> Voltar</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary next-step pull-right"><span class="glyphicon glyphicon-ok"></span> Finalizar pedido</button>
                                    </div>
                                </div>    
                            @endif
                        </div>

                        <div class="tab-pane" role="tabpanel" id="step3">
                            <div class="box-texto">CONFIRMAÇÃO</div>

                            <div class="col-md-6">
                                <ul class="list-inline pull-left">
                                    <li><a href="{{ url('\produtos') }}" class="btn btn-warning "><span class="glyphicon glyphicon-arrow-left"></span> Continuar comprando</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-inline pull-right">
                                    <li><button type="button" class="btn btn-default prev-step">Voltar</button></li>
                                    <li><button type="button" class="btn btn-default next-step">Pular</button></li>
                                    <li><button type="button" class="btn btn-primary next-step"><span class="glyphicon glyphicon-ok"></span> Finalizar compra</button></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="complete">
                            <div class="box-texto">COMPRA CONCLUÍDA</div>
                            <p>You have successfully completed all steps.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
   </div>
</div>

@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                
                $(document).ready(function() {
                    //Initialize tooltips
                    $('.nav-tabs-wizard > li a[title]').tooltip();
                    
                    //Wizard
                    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

                        var $target = $(e.target);
                    
                        if ($target.parent().hasClass('disabled')) {
                            return false;
                        }
                    });

                    $(".next-step").click(function (e) {

                        var $active = $('.wizard .nav-tabs-wizard li.active');
                        $active.next().removeClass('disabled');
                        nextTab($active);

                    });
                    $(".prev-step").click(function (e) {

                        var $active = $('.wizard .nav-tabs-wizard li.active');
                        prevTab($active);

                    });
                })

                function nextTab(elem) {
                    $(elem).next().find('a[data-toggle="tab"]').click();
                }
                function prevTab(elem) {
                    $(elem).prev().find('a[data-toggle="tab"]').click();
                }
            </script>
@endsection