@extends('master-page')

@section('content')
  
@include( 'frames.breadcrumbs' )

<div class="container content">

    @include('frames.notificacao')

    <div class="row">

    	@include( 'frames.sidebarUsuario' )
        
        <div class="col-md-9">            
            <div class="row bloco">
                <div class="triggerAnimation animated" data-animate="fadeInLeft">                   
                    <div class="title">Pedidos</div>                       
                </div>
                <div class="row">
                	<form role="form" action="{{ url('/administracao/produto/inserir-produto') }}" method="post">
	                		<div class="col-md-1 form-group">
		                		<label>Filtro</label>
		                	</div>
		                	<div class="col-md-3 form-group">
	                			<select name="cod_categoria" class="form-control">
	                                <option value="">Todos</option>                                    
	                                <option value="">Enviados</option>                                    
	                                <option value="">Novos</option>                                    
	                                <option value="">Pendentes</option>                                    
	                            </select>
		                	</div>
		                	<div class="col-md-2 form-group">
	                			<a class="btn btn-primary" href="#">OK</a>
		                	</div> 
                	</form>                 	
                </div>
                <div class="row">
                	<div class="col-md-12">
                		<table class="table cart-table responsive-table">
                                <tr>
                                    <th>
                                         Nº Pedido
                                    </th>
                                    <th>
                                         Data
                                    </th>
                                    <th>
                                         Pagamento
                                    </th>
                                    <th>
                                         Status
                                    </th>                                   
                                </tr>
                                <tr>
                                    <td>
                                       <a href="#" class="links">001</a>
                                    </td>
                                    <td>
                                        01/01/2017
                                    </td>
                                    <td>
                                        Cartão de crédito
                                    </td>
                                    <td>
                                        Processando
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>
                                       <a href="#" class="links">895</a>
                                    </td>
                                    <td>
                                        01/05/2017
                                    </td>
                                    <td>
                                        Boleto
                                    </td>
                                    <td>
                                        Processando
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>
                                       <a href="#" class="links">555</a>
                                    </td>
                                    <td>
                                        01/01/2017
                                    </td>
                                    <td>
                                        Cartão de crédito
                                    </td>
                                    <td>
                                        Processando
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>
                                       <a href="#" class="links">555</a>
                                    </td>
                                    <td>
                                        01/01/2017
                                    </td>
                                    <td>
                                        Cartão de crédito
                                    </td>
                                    <td>
                                        Processando
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>
                                       <a href="#" class="links">555</a>
                                    </td>
                                    <td>
                                        01/01/2017
                                    </td>
                                    <td>
                                        Cartão de crédito
                                    </td>
                                    <td>
                                        Processando
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td>
                                       <a href="#" class="links">555</a>
                                    </td>
                                    <td>
                                        01/01/2017
                                    </td>
                                    <td>
                                        Cartão de crédito
                                    </td>
                                    <td>
                                        Processando
                                    </td>                                    
                                </tr>
                            </table>
                	</div>  	
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                });
            </script>
@endsection