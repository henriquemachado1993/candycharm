@extends('master-page-login')

@section('content')
    <div class="top-content">    
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1 class="titulo-shadow"><strong>CandyCharm</strong> Redefinir Senha</h1>                 
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>DADOS PESSOAIS</h3>
                                <p>E-mail é obrigatorio</p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-envelope"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <!-- Exibe alguma msg -->
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form role="form" action="{{ route('password.email') }}" method="post" class="login-form smart-form">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="sr-only" for="email">E-mail</label>
                                    <input type="email" name="email" placeholder="E-mail" class="form-username form-control" id="email" value="{{old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <button type="submit" class="btn">
                                    Enviar link de redefinição de senha
                                </button>
                                <a class="btn btn-link" href="{{ route('login') }}">
                                    Login
                                </a>
                                <a class="btn btn-link" href="{{ route('register') }}">
                                    Registrar-se
                                </a>
                            </form>
                        </div>
                    </div>                   
                </div>
                @include('auth.redeSocial')
            </div>
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                
                });
            </script>
@endsection