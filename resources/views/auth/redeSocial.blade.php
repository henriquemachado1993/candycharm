<div class="row">
    <div class="col-sm-6 col-sm-offset-3 social-login">                        
        <div>
            <a title="Twitter" class="btn btn-link-2" href="#">
                <i class="fa fa-twitter"></i>
            </a>
            <a title="Instagram" class="btn btn-link-2" target="_blank" href="https://www.instagram.com/encantodedoce_/?hl=pt-br">
                <i class="fa fa-instagram"></i>
            </a>
            <a title="Facebook" class="btn btn-link-2" href="#">
                <i class="fa fa-facebook-square"></i>
            </a>
        </div>    
    </div>
</div>