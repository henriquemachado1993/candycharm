@extends('master-page-login')

@section('content')
    <div class="top-content">    
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1><strong>CandyCharm</strong> Registrar-se</h1>
                        <!-- <div class="description">
                            <p>
                                Colocar alguma descrição aqui se quiser
                            </p>
                        </div> -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>DADOS PARA CADASTRO</h3>
                                <p>Faça agora o seu cadastro, é simples!</p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-plus-square"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form role="form" action="{{ route('register') }}" method="post" class="login-form smart-form form-horizontal">
                                {{ csrf_field() }}
                                        
                                <div class="form-group{{ $errors->has('primeiro_nom_usuario') ? ' has-error' : '' }}">
                                    <div class="col-sm-6">
                                        <label class="sr-only" for="primeiro_nom_usuario">Primeiro nome</label>
                                        <input type="text" name="primeiro_nom_usuario" placeholder="Primeiro nome" class="form-username form-control" id="primeiro_nom_usuario" value="{{ old('primeiro_nom_usuario') }}" required autofocus>
                                        @if ($errors->has('primeiro_nom_usuario'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('primeiro_nom_usuario') }}</strong>
                                            </span>
                                        @endif                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="sr-only" for="complemento_nom_usuario">Último nome</label>
                                        <input type="text" name="complemento_nom_usuario" placeholder="Último nome" class="form-username form-control" id="complemento_nom_usuario" value="{{ old('complemento_nom_usuario') }}" required autofocus>
                                        @if ($errors->has('complemento_nom_usuario'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('complemento_nom_usuario') }}</strong>
                                            </span>
                                        @endif                                        
                                    </div>                                    
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-sm-12">
                                        <label class="sr-only" for="email">E-mail</label>
                                        <input type="email" name="email" placeholder="E-mail" class="form-username form-control" id="email" value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-sm-12">
                                        <label for="password" class="sr-only">Senha</label>
                                        <input type="password" name="password" placeholder="Senha" class="form-username form-control" id="password" required autofocus>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="password-confirm" class="sr-only">Confirmar senha</label>
                                        <input type="password" name="password_confirmation" placeholder="Confirmar senha" class="form-username form-control" id="password-confirm" required autofocus> 
                                    </div>                                  
                                </div>
                    
                                <button type="submit" class="btn">
                                    Registrar
                                </button>
                                <a class="btn btn-link" href="{{ route('login') }}">
                                    Login
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 social-login">                        
                        <div>
                            <a title="Twitter" class="btn btn-link-2" href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a title="Instagram" class="btn btn-link-2" href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a title="Facebook" class="btn btn-link-2" href="#">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                
                });
            </script>
@endsection