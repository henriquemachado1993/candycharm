@extends('master-page-login')

@section('content')
    <div class="top-content">    
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1 class="titulo-shadow"><strong>CandyCharm</strong> Login</h1>
                        <!-- <div class="description">
                            <p>
                                Colocar alguma descrição aqui se quiser
                            </p>
                        </div> -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>DADOS PESSOAIS</h3>
                                <p>E-mail e Senha obrigatórios para o login</p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form role="form" action="{{ route('login') }}" method="post" class="login-form smart-form">
                                {{ csrf_field() }}
                                
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="sr-only" for="email">E-mail</label>
                                    <input type="email" name="email" placeholder="E-mail" class="form-username form-control" id="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="sr-only" for="password">Senha</label>
                                    <input type="password" name="password" placeholder="Senha" class="form-password form-control" id="password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="checkbox style-0" name="remember" {{ old('remember') ? 'checked' : '' }}> Manter a sessão aberta
                                            </label>
                                        </div>
                                    </div>
                                </div>
                    
                                <button type="submit" class="btn">
                                    Entrar
                                </button>
                                <a class="btn btn-link" href="{{ route('register') }}">
                                    Registrar-se
                                </a>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Esqueceu sua senha?
                                </a>
                            </form>
                        </div>
                    </div>                   
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 social-login">                        
                        <div>
                            <a title="Twitter" class="btn btn-link-2" href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a title="Instagram" class="btn btn-link-2" href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a title="Facebook" class="btn btn-link-2" href="#">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                
                });
            </script>
@endsection