@extends('master-page-login')

@section('content')
    <div class="wrapper">
        <div class="header header-filter">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                        <div class="card card-signup">
                            <form class="form"  action="{{ route('login') }}" method="post">
                            {{ csrf_field() }}
                                <div class="header header-primary text-center">
                                    <h4>CandyCharm - Login</h4>
                                    <div class="social-line">
                                        <a href="#pablo" class="btn btn-simple btn-just-icon">
                                            <i class="fa fa-facebook-square"></i>
                                        </a>
                                        <a href="#pablo" class="btn btn-simple btn-just-icon">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a href="#pablo" class="btn btn-simple btn-just-icon">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <p class="text-divider">E-mail e Senha obrigatórios para o login</p>
                                <div class="content">

                                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>

                                        @if ($errors->has('email'))
                                            <label class="control-label">{{ $errors->first('email') }}</label>
                                        @endif
                                        
                                        <input type="text" class="form-control" placeholder="E-mail..." id="email" name="email" value="{{ old('email') }}" >
                                       
                                    </div>

                                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                        
                                        @if ($errors->has('password'))
                                            <label class="control-label">{{ $errors->first('email') }}</label>
                                        @endif

                                        <input type="password" class="form-control" placeholder="Senha..." name="password" id="password" />
                                    </div>

                                    <!-- If you want to add a checkbox to this form, uncomment this code -->

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} checked>
                                            Manter a sessão aberta
                                        </label>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-simple btn-primary btn-lg">Entrar</button>
                                </div>
                                <div class="footer text-center mrg-15px">
                                    <a href="{{ route('register') }}">
                                        <span class="simple-text">Registrar-se</span>
                                    </a>         
                                    <span class="simple-text"> | </span>                            
                                    <a href="{{ route('password.request') }}">
                                        <span class="simple-text">Esqueceu sua senha?</span>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section( 'dependencyJs' )
            <script type="text/javascript">
                $(document).ready(function() {
                
                });
            </script>
@endsection