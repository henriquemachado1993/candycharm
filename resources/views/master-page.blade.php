<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>CandyCharm</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Carregamento dos scripts CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('/css')}}/bootstrap.min.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{url('/layoutCandyCharm')}}/css/styles-candycharm.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{url('/css')}}/font-awesome.min.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{url('/css')}}/styles-flexslider.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{url('/css')}}/styles-responsive.css" media="screen"/>

    <link type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' >
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Lobster'>

    <link rel="stylesheet" type="text/css" href="{{url('/css')}}/styles-product-slider.css"  media="screen"/>
    
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="{{ url('/imagens') }}/favicon.png" type="image/x-icon">
    <link rel="icon" href="{{ url('/imagens') }}/favicon.png" type="image/x-icon">
     
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>

<body>
    <!-- Topo -->
    <div class="top-strip">
        <div class="container">
            <div class="row">
                <div class="col-md-12 top-strip-geral">

                    @if (Auth::check())
                        <ul>
                            <li>
                                <a href="#" id="btnModalBusca"><i class="fa fa-search"></i> Busque aqui... </a>
                            </li>
                            <li>

                                @php($cartCollection = Cart::getContent())
                                @php($totalItens = $cartCollection->count())
                                 
                                <a href="{{ url('/carrinho') }}"><i class="fa fa-shopping-cart"></i> Carrinho ( {{ $totalItens }} Itens ) </a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-user"></i> Olá, {{ Auth::user()->primeiro_nom_usuario }}!</a>
                            </li>                            
                            <li> 
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> Sair
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>                                
                        </ul>            
                    @else
                        <ul >
                            <li>
                                <a href="#" id="btnModalBusca"><i class="fa fa-search"></i> Busque aqui... </a>
                            </li>
                            <li>

                                @php($cartCollection = Cart::getContent())
                                @php($totalItens = $cartCollection->count())
                                 
                                <a href="{{ url('/carrinho') }}"><i class="fa fa-shopping-cart"></i> Carrinho ( {{ $totalItens }} Itens ) </a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-user"></i> Olá, Visitante!</a>
                            </li>        
                            <li><a href="{{ route('login') }}"><i class="fa fa-user"></i> Login</a></li>
                            <li><a href="{{ route('register') }}">Registrar-se</a></li>
                        </ul>                    
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Fim topo -->

    <!-- Menu -->
    <div class="container header">
        <nav id="myNavbar" class="navbar navbar-default" role="navigation">
            <div class="container">
                
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header">               
                    <a class="navbar-brand" href="#"><img src="{{ url('/layoutCandyCharm/img/candycharm') }}/logo.png" alt="logo"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li @if($objReturn['codPagina'] == "home") ? class="active" : "" @endif ><a title="Página inicial" href="{{ url('/') }}">Home</a></li>
                        <li @if($objReturn['codPagina'] == "produtos") ? class="active" : "" @endif><a title="Todos os produtos" href="{{ url('/produtos') }}">Produtos</a></li>
                        <li @if($objReturn['codPagina'] == "meusPedidos") ? class="active" : "" @endif><a title="Meus pedidos" href="{{ url('/meus-pedidos') }}">Meus pedidos</a></li>                        
                        <li @if($objReturn['codPagina'] == "sobreNos") ? class="active" : "" @endif><a title="Sobre nós" href="{{ url('/sobre-nos') }}">Sobre nós</a></li>
                        <li @if($objReturn['codPagina'] == "sac") ? class="active" : "" @endif><a title="Serviço de atendimento ao cliente" href="{{ url('/sac') }}">SAC</a></li>
                        <li>
                            <a title="Área administrativa" href="{{ url('/administracao') }}">Administração</a>                           
                        </li>                
                    </ul>
                </div>                
            </div>
        </nav>
    </div>
    <!-- Fim menu -->

<!-- Conteúdo principal --> 
@yield('pageheader')
@yield( 'content' )
<!-- Fim conteúdo principal --> 
    
    <!-- SORTEIO COLOCAR AQUI
    <div class="prize">
        <div class="container">
            <h1>win our special prize on our facebook page</h1>
        </div>
    </div>
    end-prize-->

    <!-- Roda pé -->
    <div class="full-footer">
        <div class="container-full">
            <div class="container info">
                <div class="row">
                    <div class="col-md-4 addres">
                        <!-- <img src="images/footer-logo.png" alt="logo"> -->
                        <h6>CandyCharm Webshop</h6>
                        <p>
                            Brasilia - DF
                        </p>
                        <p>
                            candycharmltda@gmail.com
                        </p>
                        <p>
                             <i class="fa fa-phone"></i> (61) 3358-2256
                        </p>
                    </div>
                    <div class="col-md-2 account">
                        <h4>Minha Conta</h4>
                        <ul>
                            <li><a href="#">Carrinho</a></li>
                            <li><a href="#">Minha conta</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 assistance">
                        <h4>Informações</h4>
                        <ul>
                            <li><a href="#">Assistência Cliente</a></li>
                            <li><a href="#">Informações de entrega</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 about">
                        <h4>Sobre Nós</h4>
                        <p>
                             Lorem ipsum dolor sit amet,consectetur adipiscing elit. in non vestibulum massa,sit amet bibendum eros. in accumsan dignissin.lorem ipsum dolor sit amet vestibulum massa,sit amet bibendum eros. in accumsan dignissin.lorem ipsum dolor sit amet
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row bottom-strip">
                <div class="col-md-6 rights">
                    <p>
                         CandyCharm - Todos os direitos reservados © 2017
                    </p>
                </div>
                <div class="col-md-6 social">
                    <ul>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Fim roda pé -->
    
    <!-- Modal Busca-->
    <div class="modal fade" id="modalBuscarPrincipal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <form role="form" action="{{ url('/resultado-busca') }}" method="post">
            {{ csrf_field() }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"></h4><i class="fa fa-search"></i> Buscar produtos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" placeholder="O que deseja pesquisar?" name="textoBusca" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">                    
                        <button type="submit" class="btn btn-primary">OK</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- Carregamento dos scripts JS -->
    <script type="text/javascript" src="{{url('/js')}}/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="{{url('/js')}}/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{url('/js')}}/modernizr.js"></script>
    <script type="text/javascript" src="{{url('/js')}}/jquery.flexslider-min.js"></script>

    <!-- Scripts para mascaras de input -->
    <script type="text/javascript" src="{{url('/js')}}/jquery.maskMoney.min.js"></script>
    <script type="text/javascript" src="{{url('/js')}}/jquery.maskedinput.min.js"></script>

    <script type="text/javascript" src="{{url('/js')}}/genericScripts.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            $('#btnModalBusca').click(function(){
                $('#modalBuscarPrincipal').modal('show');
            });
        });
    </script>
    @yield( 'dependencyJs' )

</body>
</html>