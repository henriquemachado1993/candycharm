 <div class="col-md-3 shop-sidebar">
    <div class="sidebar-widgets">               
        <div class="row right-inf">
            <h4><i class="fa fa-th-list fa-fw"></i> Categorias</h4>
            <ul>
                @if( count($objReturn['categoria']) > 0 )
                    @foreach( $objReturn['categoria'] as $v )
                        @if( !is_null($v->cod_categoria) )
                            <li><a href="{{ url('/produtos/categoria') }}/{{ $v->cod_categoria }}"><i class="fa fa-caret-right"></i> {{ $v->nom_categoria }} ({{$v->total}})</a>                
                            </li>
                        @endif
                    @endforeach                    
                @else
                    <li>
                       Nenhuma categoria
                    </li>
                @endif
            </ul>
        </div>
        <div class="row right-inf">
            <h4><i class="fa fa-shopping-basket fa-fw"></i> Produtos em Destaque</h4>
             <ul class="popular-product">
                <li>
                <img alt="" src="{{ url('/layoutCandyCharm/img/candycharm') }}/product1.jpg">
                <div>
                    <h6><a href="#">Iphone 5 black</a></h6>
                    <span>R$ 766,00</span>
                </div>
                </li>
                <li>
                <img alt="" src="{{ url('/layoutCandyCharm/img/candycharm') }}/product1.jpg">
                <div>
                    <h6><a href="#">Samasung Galaxy note 3</a></h6>
                    <span>R$ 766,00</span>
                </div>
                </li>
                <li>
                <img alt="" src="{{ url('/layoutCandyCharm/img/candycharm') }}/product1.jpg">
                <div>
                    <h6><a href="#">Iphone 5 black</a></h6>
                    <span>R$ 766,00</span>
                </div>
                </li>
                <li>
                <img alt="" src="{{ url('/layoutCandyCharm/img/candycharm') }}/product1.jpg">
                <div>
                    <h6><a href="#">Samasung Galaxy note 3</a></h6>
                    <span>R$ 766,00</span>
                </div>
                </li>
            </ul>               
        </div>
    </div>
</div>

