<div class="col-md-3 shop-sidebar">
    <div class="sidebar-widgets">               
        <div class="row right-inf">
            <h4><i class="fa fa-user"></i> Dados e Endereço</h4>
            <ul>
                <li><a href="{{url("/alterar-senha")}}"><i class="fa fa-caret-right"></i> Alterar senha</a></li>
                <li><a href="{{url("/meus-dados")}}"><i class="fa fa-caret-right"></i> Dados cadastrais</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i> Incluir novo endereço</a></li>
                <li><a href="#"><i class="fa fa-caret-right"></i> Edereços de entrega</a></li>
            </ul>
        </div>
        <div class="row right-inf">
            <h4><i class="fa fa-comments-o"></i> Atendimento ao cliente</h4>
            <ul>
                <li>
                    <p><i class="fa fa-caret-right"></i> Telefone:</p>	                        
                    <p>(36) 3354-5566</p><br>	                        
                    <p><i class="fa fa-caret-right"></i> E-mail:</p>	                        
                    <p>candycharmltda@gmail.com</p>	                        
                </li>                        
            </ul>                    
        </div>
    </div>
</div>