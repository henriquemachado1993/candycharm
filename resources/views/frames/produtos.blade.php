<div class="col-md-9 shop-section">
    <div class="row articles">
        @if( count($objReturn['produto']) > 0 )
            @foreach( $objReturn['produto'] as $v )
                <div class="col-md-4 col-sm-6">
                    <div class="product">
                        <!-- TODO: arrumar a questão para mostrar imagem que vem do banco -->
                        <img src="{{ url('/imagens/produtos') }}/product1.jpg" alt="">
                        <div class="text">
                            <a class="link-product" title="{{ $v->nom_produto }}" href="{{ url('/produto') }}/{{$v->cod_produto}}">
                                <span>
                                    {{ $v->nom_produto }}
                                </span>                              
                                <p class="vlr-produto"> {{  'R$ '.number_format($v->vlr_produto, 2, ',', '.') }}</p>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-md-12">
                <p>
                    Nenhum produto encontrado
                </p>
            </div>      
        @endif       
    </div>    
    <div class="row">
        <div class="col-md-12 latest"> 
            {{ $objReturn['produto']->links() }}
        </div>
    </div>
</div>