@if(Session::has('message'))
    <div class="alert {{ Session::get('type') }} alert-dismissable">{{ Session::get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    </div>
@endif