<!-- Banner slider -->
<div class="main-flexslider">
    <ul class="slides">
        <li class="slides"><img src="{{ url('/imagens/banners') }}/slide-01.jpg" alt="slide 01">
        <div class="slide-caption">
            <h2>Os melhores doces que você provou</h2>
            <h2><a href="#">VER OFERTA</a></h2>
        </div>
        </li>
        <li class="slides"><img src="{{ url('/imagens/banners') }}/slide-02.jpg" alt="slide 01">
        <div class="slide-caption">
            <h2>Os melhores doces que você provou</h2>
            <h2><a href="#">VER OFERTA</a></h2>
        </div>
        </li>            
    </ul>

</div>
<!-- Fim Banner slider -->