<?php

/*
| --------------------------------------------------------------------------
| Web Rotas
| --------------------------------------------------------------------------
|
| Aqui é onde você pode registrar rotas da web para sua aplicação. Estes
| Rotas são carregadas pelo RouteServiceProvider dentro de um grupo que
| Contém o grupo de middleware "web". Agora crie algo grande!
|
*/

/**************************************** CandyCharm *******************************************/

Auth::routes();

Route::any('/', [
            'as' => 'candycharm.inicio',
            'uses' => 'HomeController@index'
]);

//Carrega todos os produtos
Route::any('/resultado-busca', [
            'as' => 'candycharm.resultado-busca',
            'uses' => 'candyCharm\produtoController@buscaPrincipal'
]);

//Carrega todos os produtos
Route::any('/produtos', [
            'as' => 'candycharm.produtos',
            'uses' => 'candyCharm\produtoController@getProdutos'
]);

//Carrega um produtos de uma determinada categoria
Route::any('/produtos/categoria/{codCategoria?}', [
            'as' => 'candycharm.produtos.categoria',
            'uses' => 'candyCharm\produtoController@getProdutos'
]);

//Carrega um produto específico
Route::any('/produto/{codProduto?}', [
            'as' => 'candycharm.produto',
            'uses' => 'candyCharm\produtoController@getProdutoDetalhe'
]);

//Carrega carrinho de compras
Route::any('/carrinho', [
            'as' => 'candycharm.carrinho',
            'uses' => 'candyCharm\carrinhoController@getCarrinho'
]);

Route::group(['prefix' => '/carrinho'], function () {

      //Adiciona produto ao carrinho
      Route::any('/adicionar-produto', [
            'as' => 'candycharm.carrinho.adicionar-produto',
            'uses' => 'candyCharm\carrinhoController@adicionaProdutoCarrinho'
      ]);

      //Adiciona produto ao carrinho
      Route::any('/remover-produto/{id}', [
            'as' => 'candycharm.carrinho.remover-produto',
            'uses' => 'candyCharm\carrinhoController@removerProdutoCarrinho'
      ])->where('id', '[0-9]+');

      //Adiciona produto ao carrinho
      Route::post('/altera-qnt-produto', [
            'as' => 'candycharm.carrinho.altera-qnt-produto',
            'uses' => 'candyCharm\carrinhoController@alteraQntProdutoCarrinho'
      ]);

      Route::any('/limpar', [
            'as' => 'candycharm.carrinho.limpar',
            'uses' => 'candyCharm\carrinhoController@limparCarrinho'
      ]);

      Route::post('/aplica-cupom', [
            'as' => 'candycharm.carrinho.aplica-cupom',
            'uses' => 'candyCharm\carrinhoController@aplicarCupom'
      ]);

});

Route::any('/meus-pedidos', [
            'as' => 'candycharm.meus-pedidos',
            'uses' => 'candyCharm\usuarioController@getMeusPedidos'
]);

Route::any('/alterar-senha', [
            'as' => 'candycharm.alterar-senha',
            'uses' => 'candyCharm\usuarioController@getAlterarSenha'
]);

Route::any('/alterar-senha-usuario', [
            'as' => 'candycharm.alterar-senha-usuario',
            'uses' => 'candyCharm\usuarioController@AlterarSenha'
]);

Route::any('/meus-dados', [
            'as' => 'candycharm.meus-dados',
            'uses' => 'candyCharm\usuarioController@getMeusDados'
]);

Route::any('/sobre-nos', [
            'as' => 'candycharm.sobre-nos',
            'uses' => 'candyCharm\empresaController@getSobreNos'
]);

Route::any('/sac', [
            'as' => 'candycharm.sac',
            'uses' => 'candyCharm\empresaController@getSac'
]);

Route::any('/sac/enviar-email', [
            'as' => 'candycharm.sac.enviar-email',
            'uses' => 'candyCharm\empresaController@enviarEmail'
]);

/********************************* CandyCharm Administracao ***************************************/

Route::any('/administracao', [
            'as' => 'administracao',
            'uses' => 'homeController@indexAdmin'
]);

      //Rotas  da parte de  produto
Route::group(['prefix' => '/administracao/produto'], function () {
      
      Route::any('/inserir', [
            'as' => 'administracao.produto.inserir',
            'uses' => 'candyCharmAdmin\produtoController@getInserir'
      ]);

      Route::any('/alterar', [
            'as' => 'administracao.produto.alterar',
            'uses' => 'candyCharmAdmin\produtoController@getAlterar'
      ]);

      Route::any('/detalhe/{id?}', [
            'as' => 'administracao.produto.detalhe',
            'uses' => 'candyCharmAdmin\produtoController@getDetalhe'
      ])->where('id', '[0-9]+');

      Route::any('/lista-produtos', [
            'as' => 'administracao.produto.lista-produtos',
            'uses' => 'candyCharmAdmin\produtoController@getListaProdutos'
      ]);

      Route::any('/lista-produtos.json', [
            'as' => 'administracao.produto.lista-produtos.json',
            'uses' => 'candyCharmAdmin\produtoController@getListaProdutosJson'
      ]);

      //Inserção e alterações
      Route::any('/inserir-produto', [
            'as' => 'administracao.produto.inserir-produto',
            'uses' => 'candyCharmAdmin\produtoController@inserirProduto'
      ]);
});

      //Rotas da parte de categoria
Route::group(['prefix' => '/administracao/categoria'], function () {
      
      //Exibir paginas
      Route::any('/inserir', [
            'as' => 'administracao.categoria.inserir',
            'uses' => 'candyCharmAdmin\categoriaController@getInserir'
      ]);

      Route::any('/alterar/{id?}', [
            'as' => 'administracao.categoria.alterar',
            'uses' => 'candyCharmAdmin\categoriaController@getAlterar'
      ])->where('id', '[0-9]+');

      Route::any('/detalhe/{id?}', [
            'as' => 'administracao.categoria.detalhe',
            'uses' => 'candyCharmAdmin\categoriaController@getDetalhe'
      ])->where('id', '[0-9]+');

      Route::any('/lista-categorias', [
            'as' => 'administracao.categoria.lista-categorias',
            'uses' => 'candyCharmAdmin\categoriaController@getListaCategorias'
      ]);

      Route::any('/lista-categorias.json', [
            'as' => 'administracao.categoria.lista-categorias.json',
            'uses' => 'candyCharmAdmin\categoriaController@getListaCategoriasJson'
      ]);

      //Inserção e alterações
      Route::any('/inserir-categoria', [
            'as' => 'administracao.categoria.inserir-categoria',
            'uses' => 'candyCharmAdmin\categoriaController@inserirCategoria'
      ]);

      Route::any('/alterar-categoria/{id?}', [
            'as' => 'administracao.categoria.alterar-categoria',
            'uses' => 'candyCharmAdmin\categoriaController@alterarCategoria'
      ])->where('id', '[0-9]+');

      Route::any('/excluir-categoria/{id?}', [
            'as' => 'administracao.categoria.excluir-categoria',
            'uses' => 'candyCharmAdmin\categoriaController@excluirCategoria'
      ])->where('id', '[0-9]+');     
});

      //Rotas Cupom de desconto
Route::group(['prefix' => '/administracao/cupom'], function () {
      
      Route::any('/inserir', [
            'as' => 'administracao.cupom.inserir',
            'uses' => 'candyCharmAdmin\cupomController@getInserir'
      ]);

      Route::any('/alterar/{id?}', [
            'as' => 'administracao.cupom.alterar',
            'uses' => 'candyCharmAdmin\cupomController@getAlterar'
      ])->where('id', '[0-9]+');

      Route::any('/detalhe/{id?}', [
            'as' => 'administracao.cupom.detalhe',
            'uses' => 'candyCharmAdmin\cupomController@getDetalhe'
      ])->where('id', '[0-9]+');

      Route::any('/lista-cupons', [
            'as' => 'administracao.cupom.lista-cupons',
            'uses' => 'candyCharmAdmin\cupomController@getListaCupons'
      ]);

      Route::any('/lista-cupons.json', [
            'as' => 'administracao.cupom.lista-cupons.json',
            'uses' => 'candyCharmAdmin\cupomController@getListaCuponsJson'
      ]);

      //Inserção e alterações
      Route::any('/inserir-cupom', [
            'as' => 'administracao.cupom.inserir-cupom',
            'uses' => 'candyCharmAdmin\cupomController@inserirCupom'
      ]);

      Route::any('/alterar-cupom/{id?}', [
            'as' => 'administracao.cupom.alterar-cupom',
            'uses' => 'candyCharmAdmin\cupomController@alterarCupom'
      ])->where('id', '[0-9]+');

      Route::any('/excluir-cupom/{id?}', [
            'as' => 'administracao.cupom.excluir-cupom',
            'uses' => 'candyCharmAdmin\cupomController@excluirCupom'
      ])->where('id', '[0-9]+');  
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
