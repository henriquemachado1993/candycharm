<?php

return [
    /*  config emails */
    'email' => [
        'enderecoEmail' => "candycharmltda@gmail.com",
        'tituloEmail'   => "SAC - Serviço de atencimento ao cliente"
    ],

    /*  MENSAGENS DE ERRO GENÉRICA
    *   Para usar: config('constants.msg.insertSuccess')
    */
    'msg' => [
        'emailSuccess'  => ['msg' => 'Email enviado com sucesso!', 'type' => 'success'],

        'insertSuccess' => ['msg' => 'Registro inserido com sucesso!', 'type' => 'success'],
        'updateSuccess' => ['msg' => 'Registro alterado com sucesso!', 'type' => 'success'],
        'deleteSuccess' => ['msg' => 'Registro excluído com sucesso!', 'type' => 'success'],

        'insertError'   => ['msg' => 'Não foi possível inserir o registro!', 'type' => 'danger'],
        'updateError'   => ['msg' => 'Não foi possível alterar o registro!', 'type' => 'danger'],
        'deleteError'   => ['msg' => 'Não foi possível excluir o registro!', 'type' => 'danger'],

        'regNotFound'   => ['msg' => 'Nenhum registro foi encontrado!', 'type' => 'warning']
    ],
];